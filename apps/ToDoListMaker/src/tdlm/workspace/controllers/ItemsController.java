package tdlm.workspace.controllers;

import static djf.AppPropertyType.NEW_WARNING_CONTENT;
import static djf.AppPropertyType.NEW_WARNING_TITLE;
import java.time.LocalDate;
import java.util.ArrayList;
import tdlm.ToDoListMakerApp;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;
import tdlm.workspace.dialogs.ToDoListItemDialog;
import tdlm.transactions.AddItem_Transaction;
import tdlm.transactions.EditItem_Transaction;
import tdlm.transactions.MoveDown_Transaction;
import tdlm.transactions.MoveUpItems_Transaction;
import tdlm.transactions.RemoveItems_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsController {
    ToDoListMakerApp app;
    ToDoListItemDialog itemDialog;
    
    public ItemsController(ToDoListMakerApp initApp) {
        app = initApp;
        
        itemDialog = new ToDoListItemDialog(app);
    }
    
    public void processAddItem() {
        itemDialog.showAddDialog();
        ToDoItemPrototype newItem = itemDialog.getNewItem();        
        if (newItem != null) {
            // IF IT HAS A UNIQUE NAME AND COLOR
            // THEN CREATE A TRANSACTION FOR IT
            // AND ADD IT
            ToDoData data = (ToDoData)app.getDataComponent();
            AddItem_Transaction transaction = new AddItem_Transaction(data, newItem);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
        }    
        // OTHERWISE TELL THE USER WHAT THEY
        // HAVE DONE WRONG
        else {
            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), NEW_WARNING_TITLE, NEW_WARNING_CONTENT);
        }
    }
    
    public void processRemoveItems() {
        ToDoData data = (ToDoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<ToDoItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, itemsToRemove);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
        }
    }
    
    public void processEditItems(){
        ToDoData data = (ToDoData)app.getDataComponent();
        ToDoItemPrototype temp;
        
        if (data.isItemSelected()){
           temp = data.getSelectedItem();
           
           itemDialog.showEditDialog(temp);
           
           ToDoItemPrototype afterEdit = itemDialog.getEditItem();
           
           int i = data.getItemIndex(data.getSelectedItem());
           
           if(afterEdit!=null){
                EditItem_Transaction transaction = new EditItem_Transaction(data,temp,afterEdit,i);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
           }
           
           data.clearSelected();
           data.selectItem(afterEdit);     
        }
    }
    
    
    public void processMoveUpItems(){
        ToDoData data = (ToDoData)app.getDataComponent();
        if (data.isItemSelected()){
            int i = data.getItemIndex(data.getSelectedItem());
            
            if(data.getItemIndex(data.getSelectedItem())!=0){
                ToDoItemPrototype itemsToPaste = data.getSelectedItem();
//                data.removeItem(data.getSelectedItem());
//                data.addItemAt(itemsToPaste, i-1);
                MoveUpItems_Transaction transaction = new MoveUpItems_Transaction(data,itemsToPaste,i);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
                
                
//                data.clearSelected();
//                data.selectItem(itemsToPaste);
                app.getFoolproofModule().updateAll();
                app.getFileModule().markAsEdited(true);
            }
 
            //System.out.print(data.getSelectedItem().getCategory() + "222");
        }
    }
    
    public void processMoveDownItems(){
        ToDoData data = (ToDoData)app.getDataComponent();
        if (data.isItemSelected()){
            int i = data.getItemIndex(data.getSelectedItem());
            if(data.getItemIndex(data.getSelectedItem())!= data.getItems().size()-1){
                ToDoItemPrototype itemsToPaste = data.getSelectedItem();
//                data.removeItem(data.getSelectedItem());
//                data.addItemAt(itemsToPaste, i+1);

                MoveDown_Transaction transaction = new MoveDown_Transaction(data,itemsToPaste,i);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
                
//                data.clearSelected();
//                data.selectItem(itemsToPaste);
                app.getFoolproofModule().updateAll();
                app.getFileModule().markAsEdited(true);
                
            }
 
            //System.out.print(data.getSelectedItem().getCategory() + "222");
        }
    }
    
}
