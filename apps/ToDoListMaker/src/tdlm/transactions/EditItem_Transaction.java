/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm.transactions;

import jtps.jTPS_Transaction;
import tdlm.ToDoListMakerApp;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;

/**
 *
 * @author liuxiao
 */
public class EditItem_Transaction implements jTPS_Transaction  {//想清楚怎么建class和class的constructure，class的属性
    ToDoData data;
    ToDoItemPrototype afterE;
    ToDoItemPrototype temp;
    int dex;
    
    public EditItem_Transaction(ToDoData initData,ToDoItemPrototype initTemp, ToDoItemPrototype initAfter, int initIndex){//, ToDoListMakerApp initApp){
        this.data = initData;
        this.temp = initTemp;
        this.afterE = initAfter;
        this.dex = initIndex;
    }
    
    @Override
    public void doTransaction() {
        data.removeItem(temp);
        data.addItemAt(afterE, dex);
    }

    @Override
    public void undoTransaction() {
        data.removeItem(afterE);
        data.addItemAt(temp, dex);
    }
    
}
