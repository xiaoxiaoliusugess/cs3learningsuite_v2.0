/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm.transactions;

import jtps.jTPS_Transaction;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;

/**
 *
 * @author liuxiao
 */
public class MoveUpItems_Transaction implements jTPS_Transaction{
    ToDoData data;
    ToDoItemPrototype temp;
    int i;

    public MoveUpItems_Transaction(ToDoData initData, ToDoItemPrototype initTemp, int initI) {
        this.data = initData;
        this.temp = initTemp;
        this.i = initI;
    }
    
    @Override
    public void doTransaction() {

            data.moveItem(i, i-1);
            data.clearSelected();
            data.selectItem(temp);
        
    }

    @Override
    public void undoTransaction() {
       
            data.moveItem(i-1, i);
            data.clearSelected();
            data.selectItem(temp);
            
    }
    
}
