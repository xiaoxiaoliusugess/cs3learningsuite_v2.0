/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.LoGoItemPropertyType;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import gologolo.workspace.LoGoWorkspace;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class MoveUpItems_Transaction implements jTPS_Transaction{
    GoLoGoLoApp app;
    LoGoData data;
    LoGoItemPrototype temp;
    int i;

    public MoveUpItems_Transaction(GoLoGoLoApp app,LoGoData initData, LoGoItemPrototype initTemp, int initI) {
        this.app = app;
        this.data = initData;
        this.temp = initTemp;
        this.i = initI;
    }
    
    @Override
    public void doTransaction() {
        
        data.removeItemInPane(temp);
        data.addItemInPaneByIndex(i-1, temp);
        data.moveItem(i, i-1);
        data.clearSelected();
        data.selectItem(temp);
        data.reSetOrder();
            
    }

    @Override
    public void undoTransaction() {
        
        data.removeItemInPane(temp);
        data.addItemInPaneByIndex(i, temp);
        data.moveItem(i-1, i);
        data.clearSelected();
        data.selectItem(temp);
        data.reSetOrder();
            
            //((LoGoWorkspace)app.getWorkspaceComponent()).getItemsController().highLightCanvasBtable();
         
            
    }
    
}
