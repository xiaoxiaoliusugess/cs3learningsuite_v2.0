/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoItemPrototype;
import javafx.scene.shape.Circle;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ResizeCircle_Transaction implements jTPS_Transaction{

    LoGoItemPrototype newItem;
    double r;
    double newR;
    
    public ResizeCircle_Transaction(LoGoItemPrototype newItem,double r){
        this.newItem = newItem;
        this.r = r; 
        this.newR = ((Circle)newItem.getNode()).getRadius();
        
    }
    
    @Override
    public void doTransaction() {
        ((Circle)newItem.getNode()).setRadius(newR);
        System.out.println("之后的半径1：" + newR);
    }

    @Override
    public void undoTransaction() {
        ((Circle)newItem.getNode()).setRadius(r);
    }
}
