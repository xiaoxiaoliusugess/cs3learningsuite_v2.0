/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoItemPrototype;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ChangeBorderTickness_Transaction implements jTPS_Transaction{
    LoGoItemPrototype item;
    double value;
    double oriValue; 
    

    public ChangeBorderTickness_Transaction(LoGoItemPrototype item, double value) {
        this.item = item;
        this.value = value;
        if(item.getType().equals("Rectangle")){
            oriValue = ((Rectangle)item.getNode()).getStrokeWidth();
        }
        else if(item.getType().equals("Circle")){
            oriValue = ((Circle)item.getNode()).getStrokeWidth();
        }
    }

    @Override
    public void doTransaction() {
        if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setStrokeWidth(value);
        }
        else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setStrokeWidth(value);
        }
    }

    @Override
    public void undoTransaction() {
         if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setStrokeWidth(oriValue);
        }
        else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setStrokeWidth(oriValue);
        }
    }
    
}
