/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoData;
import javafx.scene.shape.Rectangle;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ResizeLogo_transaction implements jTPS_Transaction {
    LoGoData data;
    double actualH;
    double actualW;
    double height;
    double width;
    
    public ResizeLogo_transaction(LoGoData data,double actH,double actW,double heit,double wei){
        this.data = data;
        this.actualH = actH;
        this.actualW = actW;
        this.height = heit;
        this.width = wei;
    }

    @Override
    public void doTransaction() {
        data.reSize(width, height);
        Rectangle forClip = new Rectangle(width,height);
        forClip.setStyle("-fx-background-color: white;");
        data.getBackground().setClip(forClip);
    }

    @Override
    public void undoTransaction() {
        data.reSize(actualW, actualH);
        Rectangle forClip = new Rectangle(actualW,actualH);
        forClip.setStyle("-fx-background-color: white;");
        data.getBackground().setClip(forClip);
    }
    
}
