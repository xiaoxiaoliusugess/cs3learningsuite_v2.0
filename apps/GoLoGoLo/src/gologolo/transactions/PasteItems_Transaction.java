/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.LoGoItemPropertyType;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import gologolo.workspace.LoGoWorkspace;
import java.util.ArrayList;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class PasteItems_Transaction implements jTPS_Transaction {
    GoLoGoLoApp app;
    LoGoItemPrototype itemsToPaste;
    LoGoData data;
    int pasteIndexBefore;
    
    public PasteItems_Transaction(  GoLoGoLoApp initApp, 
                                    LoGoItemPrototype initItemsToPaste,
                                    int initPasteIndex) {
        
        app = initApp;
        data = (LoGoData)app.getDataComponent();
        itemsToPaste = initItemsToPaste;
        pasteIndexBefore = initPasteIndex;
    }

   
    @Override
    public void doTransaction() {
        
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0))
                .getChildren().add(itemsToPaste.getNode());
        for(int i = 0; i < data.getNumItems();i++){
                       data.getItems().get(i).getNode().setEffect(null);
                   }
        data.clearSelected();
        data.addItem(itemsToPaste);
        data.selectItem(itemsToPaste);
        data.reSetOrder();
        data.highLightCanvasBtable();
                
        ((LoGoWorkspace)app.getWorkspaceComponent()).getItemsController().enableNodeHighlight(data, itemsToPaste.getNode(),itemsToPaste);
        ((LoGoWorkspace)app.getWorkspaceComponent()).getItemsController().processgrapDp(itemsToPaste.getNode(),itemsToPaste, data.getNumItems()-1);
                
                
        
    }

    @Override
    public void undoTransaction() {
        
        if(pasteIndexBefore==-1){
            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0))
                .getChildren().remove(itemsToPaste.getNode());
            data.removeItem(itemsToPaste);
            data.clearSelected();
            data.reSetOrder();
            
        }
        else{
            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0))
                .getChildren().remove(itemsToPaste.getNode());
            data.removeItem(itemsToPaste);
            data.selectItem(data.getItems().get(pasteIndexBefore));
            DropShadow ds2 = new DropShadow();
            ds2.setColor(Color.CORAL);
            data.getSelectedItem().getNode().setEffect(ds2);
            data.reSetOrder();
        }


    }   
}