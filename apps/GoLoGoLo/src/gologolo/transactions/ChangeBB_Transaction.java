/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoData;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ChangeBB_Transaction implements jTPS_Transaction{
    StackPane pane;
    RadialGradient componentFRG;
    Paint orione;

    public ChangeBB_Transaction(StackPane pane, RadialGradient componentFRG) {
        this.pane = pane;
        this.componentFRG = componentFRG;
        
        this.orione = Paint.valueOf(pane.getBackground().getFills().get(0).getFill().toString());
    }

    
    @Override
    public void doTransaction() {
        pane.setBackground(new Background(new BackgroundFill(componentFRG,null,null)));
    }

    @Override
    public void undoTransaction() {
        pane.setBackground(new Background(new BackgroundFill(orione,null,null)));
    }
    
    
    
}
