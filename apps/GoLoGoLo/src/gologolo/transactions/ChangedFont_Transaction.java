/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoItemPrototype;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ChangedFont_Transaction implements jTPS_Transaction{
    LoGoItemPrototype item;
    Font f;
    boolean changedBold;
    boolean changedItalic;
    Font oriF;

    public ChangedFont_Transaction(LoGoItemPrototype item, Font f, boolean changedBold, boolean changedItalic) {
        this.item = item;
        this.f = f;
        this.changedBold = changedBold;
        this.changedItalic = changedItalic;
        oriF = ((Text)item.getNode()).getFont();
        
    }

    @Override
    public void doTransaction() {
        ((Text)item.getNode()).setFont(f);
        if(changedBold) {
            item.setIsBold(!item.isIsBold());
        }
        if(changedItalic) {
            item.setIsItalic(!item.isIsItalic());
        }
    }

    @Override
    public void undoTransaction() {
        ((Text)item.getNode()).setFont(oriF);
        if(changedBold) {
            item.setIsBold(!item.isIsBold());
        }
        if(changedItalic) {
            item.setIsItalic(!item.isIsItalic());
        }
    }
    
}
