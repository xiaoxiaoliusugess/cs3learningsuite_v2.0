/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoItemPrototype;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ChangeBorderColor_Transaction implements jTPS_Transaction{
    LoGoItemPrototype item;
    Color color;
    Color oriColor;

    public ChangeBorderColor_Transaction(LoGoItemPrototype item, Color color) {
        this.item = item;
        this.color = color;
        
        if(item.getType().equals("Rectangle")){
            oriColor = (Color)((Rectangle)item.getNode()).getStroke();
        }
        else if(item.getType().equals("Circle")){
            oriColor = (Color)((Circle)item.getNode()).getStroke();
        }
    }

    @Override
    public void doTransaction() {
        if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setStroke(color);
        }
        else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setStroke(color);
        }
    }

    @Override
    public void undoTransaction() {
       if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setStroke(oriColor);
        }
        else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setStroke(oriColor);
        }
    }
}
