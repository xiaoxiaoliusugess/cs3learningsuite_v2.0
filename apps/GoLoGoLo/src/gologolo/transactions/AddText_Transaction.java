/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.LoGoItemPropertyType;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import gologolo.workspace.LoGoWorkspace;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class AddText_Transaction implements jTPS_Transaction{
    GoLoGoLoApp app;
    LoGoData data;
    LoGoItemPrototype itemToAdd;

    public AddText_Transaction(GoLoGoLoApp initapp,LoGoData initData, LoGoItemPrototype initNewItem) {
        app =initapp;
        data = initData;
        itemToAdd = initNewItem;
    }

    @Override
    public void doTransaction() {
        data.addItemInPane(itemToAdd);
        data.addItem(itemToAdd);
        data.dataEffectNull();
        data.clearSelected();
        data.selectItem(itemToAdd);
        data.reSetOrder();
        data.highLightCanvasBtable();
    }
        
        

    @Override
    public void undoTransaction() {
        data.removeItemInPane(itemToAdd);
        data.removeItem(itemToAdd);
        data.dataEffectNull();
        data.clearSelected();
        data.reSetOrder();
        
    }
    
}
