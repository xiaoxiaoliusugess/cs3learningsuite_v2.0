/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class AddCircle_Transaction implements jTPS_Transaction{
GoLoGoLoApp app;
    LoGoData data;
    LoGoItemPrototype itemToAdd;
    LoGoItemPrototype temp;
   

    public AddCircle_Transaction(GoLoGoLoApp initapp,LoGoData initData,LoGoItemPrototype itemToAdd) {
        app = initapp;
        data = initData;
        this.itemToAdd = itemToAdd;
        temp = data.getSelectedItem();
    }
 
    @Override
    public void doTransaction() {
        
        if(data.getNumItems()!=0){
            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().add(itemToAdd.getNode());
            data.addItem(itemToAdd);
            data.dataEffectNull();
            data.clearSelected();
            data.selectItem(itemToAdd);
            data.highLightCanvasBtable();
            data.reSetOrder();
        }
        else{
            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().add(itemToAdd.getNode());
            data.addItem(itemToAdd);
            data.selectItem(itemToAdd);
            data.reSetOrder();
        }
    }

    @Override
    public void undoTransaction() {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().remove(itemToAdd.getNode());
        
        data.removeItem(itemToAdd);
        
        data.clearSelected();
        data.reSetOrder();
        
        
        
    }
    
}
