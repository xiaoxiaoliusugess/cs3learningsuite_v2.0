/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.LoGoItemPropertyType;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class EditItem_Transaction implements jTPS_Transaction{
    LoGoData data;
    LoGoItemPrototype temp;
    String origionalstring;
    String afterstring;


    public EditItem_Transaction(LoGoData initData, LoGoItemPrototype initTemp, String afterEditText) {
       
        this.data = initData;
        this.temp = initTemp;
        this.afterstring = afterEditText;
        this.origionalstring = ((Text)temp.getNode()).getText();

        
    }

    @Override
    public void doTransaction() {
        
        ((Text)temp.getNode()).setText(afterstring);

        data.selectItem(temp);
        data.highLightCanvasBtable();
        data.reSetOrder();
        
    }

    @Override
    public void undoTransaction() {

        ((Text)temp.getNode()).setText(origionalstring);
        data.selectItem(temp);
        data.highLightCanvasBtable();
        data.reSetOrder();
    }
    
}
