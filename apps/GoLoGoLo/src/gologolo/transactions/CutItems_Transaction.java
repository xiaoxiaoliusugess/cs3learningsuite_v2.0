/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import gologolo.GoLoGoLoApp;
import gologolo.LoGoItemPropertyType;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import java.util.ArrayList;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class CutItems_Transaction implements jTPS_Transaction {
    GoLoGoLoApp app;
    LoGoItemPrototype itemsToCut;
    int cutItemLocations;
    
    public CutItems_Transaction(GoLoGoLoApp initApp, LoGoItemPrototype initItemsToCut) {
        app = initApp;
        itemsToCut = initItemsToCut;
        cutItemLocations = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0))
                .getChildren().indexOf(initItemsToCut.getNode());
        
    }
    
    @Override
    public void doTransaction() {
        LoGoData data = (LoGoData)app.getDataComponent();
        
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0))
                .getChildren().remove(itemsToCut.getNode());
       
        data.getItems().remove(itemsToCut);
        
        data.reSetOrder();
        
        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
        
        data.clearSelected();
    }

    @Override
    public void undoTransaction() {
        LoGoData data = (LoGoData)app.getDataComponent();
         ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0))
                .getChildren().add(cutItemLocations, itemsToCut.getNode());
         
        if(data.isItemSelected()){
            for(int i = 0; i < data.getNumItems();i++){
                       data.getItems().get(i).getNode().setEffect(null);
                   }
            data.clearSelected();
            data.getItems().add(cutItemLocations, itemsToCut);
            data.selectItem(itemsToCut);
//            DropShadow ds2 = new DropShadow();
//            ds2.setColor(Color.CORAL);
//            itemsToCut.getNode().setEffect(ds2);
            data.reSetOrder();
            data.highLightCanvasBtable();
            app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
        }
        else{
            data.getItems().add(cutItemLocations, itemsToCut);
            data.selectItem(itemsToCut);
//            DropShadow ds2 = new DropShadow();
//            ds2.setColor(Color.CORAL);
//            itemsToCut.getNode().setEffect(ds2);
            data.reSetOrder();
            data.highLightCanvasBtable();
            app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
        }
    }   
}
