/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class EditName_Transaction implements jTPS_Transaction {
    LoGoData data;
    LoGoItemPrototype itemToChange;
    String changedName;
    String originalName;
    
    public EditName_Transaction(LoGoData data,LoGoItemPrototype itemToChange,String orig,String after){
        this.data = data;
        this.itemToChange = itemToChange;
        this.originalName = orig;
        this.changedName = after;
    }
    

    @Override
    public void doTransaction() {
        itemToChange.setName(changedName);
    }

    @Override
    public void undoTransaction() {
        itemToChange.setName(originalName);
    }
    
}
