/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoItemPrototype;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ChangeTextColor_Transaction implements jTPS_Transaction{
    LoGoItemPrototype item;
    Color color;
    Color oriColor;

    public ChangeTextColor_Transaction(LoGoItemPrototype item, Color color) {
        this.item = item;
        this.color = color;
        oriColor = (Color)((Text)item.getNode()).getFill();
        
    }

    @Override
    public void doTransaction() {
        ((Text)item.getNode()).setFill(color);
        }

    @Override
    public void undoTransaction() {
        ((Text)item.getNode()).setFill(oriColor);
    }
    
}
