/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class grapDrap_Transaction implements jTPS_Transaction{
    
    GoLoGoLoApp app;
    LoGoItemPrototype newItem;
    double oldx,oldy;
    double newx,newy;
    int index;

    public grapDrap_Transaction(GoLoGoLoApp app,LoGoItemPrototype newItem,double oldx, double oldy, double newx, double newy,int index){
        this.app = app;
        this.newItem = newItem;
        this.oldx = oldx;
        this.oldy = oldy;
        this.newx = newx;
        this.newy = newy;
        this.index = index; 
    }

    @Override
    public void doTransaction() {
        if(newItem.getType().equals("Rectangle")){
           ((Rectangle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(newx);
           ((Rectangle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(newy);
        }
        else if(newItem.getType().equals("Text")){
            ((Text)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(newx);
           ((Text)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(newy);
        
        }
        else if(newItem.getType().equals("Image")){
            ((Node)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(newx);
           ((Node)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(newy);
        
        }
        else{
            ((Circle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(newx);
           ((Circle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(newy);
        
        }
                    
    }

    @Override
    public void undoTransaction() {
        if(newItem.getType().equals("Rectangle")){
            ((Rectangle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(oldx);
            ((Rectangle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(oldy);
        }
        else if(newItem.getType().equals("Text")){
            ((Text)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(oldx);
            ((Text)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(oldy);
            
        }
        else if(newItem.getType().equals("Image")){
            ((Node)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(oldx);
            ((Node)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(oldy);
            
        }
        else{
            ((Circle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateX(oldx);
            ((Circle)((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index)).setTranslateY(oldy);
            
        }
        
    }
    
}
