/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class Delete_Transaction implements jTPS_Transaction{
    GoLoGoLoApp app;
    LoGoData data;
    LoGoItemPrototype itemToDelete;
    int index;
    
public Delete_Transaction(GoLoGoLoApp initapp,LoGoData initData, LoGoItemPrototype initNewItem,int index){
        app = initapp;
        data = initData;
        itemToDelete = initNewItem;
        this.index = index;
}

    @Override
    public void doTransaction() {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().remove(itemToDelete.getNode());
        data.removeItem(itemToDelete);
        data.clearSelected();
        data.reSetOrder();
    }

    @Override
    public void undoTransaction() {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().add(itemToDelete.getNode());
        data.addItemAt(itemToDelete, index); 
        data.clearSelected();
        data.reSetOrder();
    }
    
}
