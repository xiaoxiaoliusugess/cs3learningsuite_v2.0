/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.LoGoItemPrototype;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ChangeColorRadius_Transaction implements jTPS_Transaction{
    LoGoItemPrototype item;
    RadialGradient componentFRG;
    RadialGradient orione;

    public ChangeColorRadius_Transaction(LoGoItemPrototype item, RadialGradient componentFRG) {
        this.item = item;
        this.componentFRG = componentFRG;
        if(item.getType().equals("Rectangle")){
            orione = (RadialGradient)((Rectangle)item.getNode()).getFill();
        }
        else if(item.getType().equals("Circle")){
            orione = (RadialGradient)((Circle)item.getNode()).getFill();
        }
    }

    @Override
    public void doTransaction() {
        if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setFill(componentFRG);
        }
        else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setFill(componentFRG);
        }
    }

    @Override
    public void undoTransaction() {
        if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setFill(orione);
        }
        else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setFill(orione);
        }
    }
    
    
}
