/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLoGoLoApp;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class AddImage_Transaction implements jTPS_Transaction{

    GoLoGoLoApp app;
    LoGoData data;
    LoGoItemPrototype itemToAdd;
    
   

    public AddImage_Transaction(GoLoGoLoApp initapp,LoGoData initData,LoGoItemPrototype iniitemToAdd) {
        app = initapp;
        data = initData;
        itemToAdd = iniitemToAdd;

    }
    
    @Override
    public void doTransaction() {
        data.addItemInPane(itemToAdd);
        data.addItem(itemToAdd);
        data.reSetOrder();
        data.clearSelected();
        data.selectItem(itemToAdd);
        data.highLightCanvasBtable();
    }

    @Override
    public void undoTransaction() {
        data.removeItemInPane(itemToAdd);
        data.removeItem(itemToAdd);
        data.reSetOrder();
        data.clearSelected();
        
    }
    
}
