/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import gologolo.GoLoGoLoApp;
import static gologolo.LoGoItemPropertyType.GLGL_ITEMS_TABLE_VIEW;
import gologolo.workspace.LoGoWorkspace;
import gologolo.workspace.controllers.ItemsController;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.TableView;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author liuxiao
 */
public class LoGoData implements AppDataComponent {
    GoLoGoLoApp app;
    ObservableList<LoGoItemPrototype> items;
    TableView.TableViewSelectionModel itemsSelectionModel;
    StackPane background; 
    SingleSelectionModel itemSelectionInpane;
    
    String url;
    
    static double orgSceneX, orgSceneY;
    static double orgTranslateX, orgTranslateY;
    static double newTranslateX,newTranslateY;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public StackPane getBackground() {
        return background;
    }

    public void setBackground(StackPane background) {
        this.background = background;
    }
    
    public void reSetOrder(){
        for(int i = 0; i < items.size(); i++){
            this.getItems().get(i).setOrder(i+1);
        }
    }
    
    public LoGoData(GoLoGoLoApp initApp) {
        app = initApp;
        background = new StackPane(); 
        
        background.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                
            if (e.getPickResult().getIntersectedNode().equals(background)) {

                for(int i = 0; i < items.size();i++){
                    items.get(i).getNode().setEffect(null);
                    clearSelected();
                }


            }
            else{

                ((LoGoWorkspace)app.getWorkspaceComponent())
                           .getItemsController().cancelHight();

//                    ((LoGoWorkspace)app.getWorkspaceComponent())
//                               .getItemsController().resizeCircleBScollM();
            }
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            }
        });
        
        background.setOnMousePressed((MouseEvent e2) -> {
            if(e2.getPickResult().getIntersectedNode().equals(background))
            {
                
                LoGoData.orgSceneX = e2.getSceneX();
                LoGoData.orgSceneY = e2.getSceneY();
                orgTranslateX = ((StackPane)(e2.getSource())).getTranslateX();
                orgTranslateY = ((StackPane)(e2.getSource())).getTranslateY();
            }
        });
        
        background.setOnMouseDragged((MouseEvent e3) -> {
            if(e3.getPickResult().getIntersectedNode().equals(background))
            {
                    double offsetX = e3.getSceneX() - orgSceneX;
                double offsetY = e3.getSceneY() - orgSceneY;

                LoGoData.newTranslateX = orgTranslateX + offsetX/background.getParent().getScaleX();
                LoGoData.newTranslateY = orgTranslateY + offsetY/background.getParent().getScaleY();

                ((StackPane)(e3.getSource())).setTranslateX(newTranslateX);
                ((StackPane)(e3.getSource())).setTranslateY(newTranslateY);
            }

        });
        
        background.setOnMouseReleased((MouseEvent e4) -> {
            if(e4.getPickResult().getIntersectedNode().equals(background))
                {
                if(newTranslateX != 0 || newTranslateY !=0){
                    background.setTranslateX(LoGoData.newTranslateX);
                    background.setTranslateY(LoGoData.newTranslateY);
                }
            }

        });
        
        background.setOnScroll(e1->{
            if(e1.getPickResult().getIntersectedNode().equals(background))
            {
                if(e1.getDeltaY()>0){
                    ((LoGoWorkspace)app.getWorkspaceComponent())
                                   .getItemsController().enlargeCanvas();
                }
                else{
                    ((LoGoWorkspace)app.getWorkspaceComponent())
                                   .getItemsController().smallCanvas();
                }
            }
        });
        
        orgSceneX = 0;
        orgSceneY = 0;
        orgTranslateX = 0;
        orgTranslateY = 0;
        newTranslateX = 0;
        newTranslateY = 0;
        
        ((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().add(background);
        
        //set clip for 最外面灰色的画布
        Rectangle forClip1 = new Rectangle();
        forClip1.heightProperty().bind(((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).heightProperty());
        forClip1.widthProperty().bind(((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).widthProperty());
        forClip1.setStyle("-fx-background-color: transparent;");
        ((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter().setClip(forClip1);


        reSize(800,500);//可以用properties表示
        if(app.getRecentWorkModule().isLoad()){
            System.out.println(app.getGUIModule().isLoaded() + " + load");
            System.out.println(app.getGUIModule().getFileController().isLoad() + " + load2");
            //background.setStyle("-fx-background-color: white;");
        }
        else{
            background.setStyle("-fx-background-color: white;");
        }
        //background.setStyle("-fx-background-color: white;");
        
        
        Rectangle forClip = new Rectangle(800,500);
        forClip.setStyle("-fx-background-color: white;");
        background.setClip(forClip);

        // GET ALL THE THINGS WE'LL NEED TO MANIUPLATE THE TABLE
        TableView tableView = (TableView) app.getGUIModule().getGUINode(GLGL_ITEMS_TABLE_VIEW);
        items = tableView.getItems();
        itemsSelectionModel = tableView.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
        
    }
    
    public void reSize(double width, double height){
        background.setMaxSize(width, height);
    }
    
    public ObservableList<LoGoItemPrototype> getItems() {
        return items;
    }

    public void setItems(ObservableList<LoGoItemPrototype> items) {
        this.items = items;
    } 
    
    public Iterator<LoGoItemPrototype> itemsIterator() {
        return this.items.iterator();
    }
    
    @Override
    public void reset() {
        AppGUIModule gui = app.getGUIModule();
        
         TableView tableView = (TableView)gui.getGUINode(GLGL_ITEMS_TABLE_VIEW);
        items = tableView.getItems();
        items.clear();
    }
        
    

    public boolean isItemSelected() {
        ObservableList<LoGoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() == 1);
    }
    
    public boolean areItemsSelected() {
        ObservableList<LoGoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() > 1);        
    }

    public boolean isValidToDoItemEdit(LoGoItemPrototype itemToEdit, String name, int order, String type, Node obj) {
        return isValidNewToDoItem(name,order,type,obj);
    }

    public boolean isValidNewToDoItem(String name, int order, String type, Node obj) {
        if (obj.toString().trim().length() == 0)
            return false;
        return true;
    }
    
    public void removeBIndex(int i ){
        items.remove(i);
    }//fa
    
    public void addItem(LoGoItemPrototype itemToAdd) {
        items.add(itemToAdd);
    }
    
    public void addItemInPane(LoGoItemPrototype itemToAdd){
        
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().add(itemToAdd.getNode());
    }
    

    public void removeItem(LoGoItemPrototype itemToAdd) {
        items.remove(itemToAdd);
        //((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().remove(itemToAdd.getNode());
    }
    
    public void removeItemInPane(LoGoItemPrototype itemToAdd) {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().remove(itemToAdd.getNode());
    }
    
    public void addItemInPaneByIndex(int index,LoGoItemPrototype itemToAdd){
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().add(index, itemToAdd.getNode());
    }

    public LoGoItemPrototype getSelectedItem() {
        if (!isItemSelected()) {
            return null;
        }
        return getSelectedItems().get(0);
    }
    
    public ObservableList<LoGoItemPrototype> getSelectedItems() {
        return (ObservableList<LoGoItemPrototype>)this.itemsSelectionModel.getSelectedItems();
    }
    
  
    public int getItemIndex(LoGoItemPrototype item) {
        
        return items.indexOf(item);
        
    }

    public void addItemAt(LoGoItemPrototype item, int itemIndex) {
        items.add(itemIndex, item);
    }

    public void moveItem(int oldIndex, int newIndex) {
        LoGoItemPrototype itemToMove = items.remove(oldIndex);
        items.add(newIndex, itemToMove);
    }
    
    public void moveItemInCanvas(int oldIndex, int newIndex){
        Node nodeToMove = items.remove(oldIndex).getNode();
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0))
                .getChildren().add(newIndex,nodeToMove);
        
     }

    public int getNumItems() {
        return items.size();
    }

    public void selectItem(LoGoItemPrototype itemToSelect) {
        this.itemsSelectionModel.select(itemToSelect);
    }

    public ArrayList<Integer> removeAll(ArrayList<LoGoItemPrototype> itemsToRemove) {
        ArrayList<Integer> itemIndices = new ArrayList();
        for (LoGoItemPrototype item: itemsToRemove) {
            itemIndices.add(items.indexOf(item));
        }
        for (LoGoItemPrototype item: itemsToRemove) {
            items.remove(item);
        }
        return itemIndices;
    }

    public void addAll(ArrayList<LoGoItemPrototype> itemsToAdd, ArrayList<Integer> addItemLocations) {
        for (int i = 0; i < itemsToAdd.size(); i++) {
            LoGoItemPrototype itemToAdd = itemsToAdd.get(i);
            Integer location = addItemLocations.get(i);
            items.add(location, itemToAdd);
        }
    }

    public ArrayList<LoGoItemPrototype> getCurrentItemsOrder() {
        ArrayList<LoGoItemPrototype> orderedItems = new ArrayList();
        for (LoGoItemPrototype item : items) {
            orderedItems.add(item);
        }
        return orderedItems;
    }

    public void clearSelected() {
        this.itemsSelectionModel.clearSelection();
    }

    public void sortItems(Comparator sortComparator) {
        Collections.sort(items, sortComparator);
    }

    public void rearrangeItems(ArrayList<LoGoItemPrototype> oldListOrder) {
        items.clear();
        for (LoGoItemPrototype item : oldListOrder) {
            items.add(item);
        }
    }
    
    public void highLightCanvasBtable(){
        for(int i = 0; i < items.size();i++){
            items.get(i).getNode().setEffect(null);
        }
        if(isItemSelected()){
            LoGoItemPrototype select = getSelectedItem();
            int index = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().indexOf(select.getNode());
            DropShadow ds2 = new DropShadow();
            ds2.setColor(Color.CORAL);
            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index).setEffect(ds2);
        }
    }
    
    public StackPane getCanvas() {
        StackPane canvas = (StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0);
        return canvas;
    }
    
    public int getNodeIndexInPane(LoGoItemPrototype prototype) {
        int index =((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().indexOf(prototype.getNode());
        return index;
    }
    
    public void dataEffectNull(){
        for(int i = 0; i < items.size();i++){
                items.get(i).getNode().setEffect(null);
            }
    }

    public void addItemWithoutTrancation(LoGoItemPrototype item) {
        addItemInPane(item);
        addItem(item);
        dataEffectNull();
        clearSelected();
        selectItem(item);
        reSetOrder();
        highLightCanvasBtable();
        ((LoGoWorkspace)app.getWorkspaceComponent()).getItemsController().processgrapDp(item.getNode(),item, this.getNumItems()-1);
        ((LoGoWorkspace)app.getWorkspaceComponent()).getItemsController().enableNodeHighlight(this, item.getNode(), item);

    }

}
