/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author liuxiao
 */
public class LoGoResizeRec {
    Circle topLeft = new Circle(0,0,7);
    Circle topRight = new Circle(0,0,7);
    Circle bottomLeft = new Circle(0,0,7);
    Circle bottomRight = new Circle(0,0,7);
    Rectangle rec = new Rectangle();
    
    public LoGoResizeRec(Circle tLeft,Circle tRight,Circle bLeft,Circle bRight,Rectangle rec){
        this.topLeft = tLeft;
        this.topRight = tRight;
        this.bottomLeft = bLeft;
        this.bottomRight = bRight;
        this.rec = rec;
        this.topLeft.setFill(Color.BLACK);
        this.topLeft.setOpacity(0.3);
        this.topLeft.setManaged(false);
        this.topRight.setFill(Color.BLACK);
        this.topRight.setOpacity(0.3);
        this.topRight.setManaged(false);
        this.bottomRight.setFill(Color.BLACK);
        this.bottomRight.setOpacity(0.3);
        this.bottomRight.setManaged(false);
        this.bottomLeft.setFill(Color.BLACK);
        this.bottomLeft.setOpacity(0.3);
        this.bottomLeft.setManaged(false);
    }
    
    
}
