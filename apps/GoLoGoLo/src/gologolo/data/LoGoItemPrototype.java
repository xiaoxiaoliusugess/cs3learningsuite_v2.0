/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
/**
 *
 * @author liuxiao
 */
public class LoGoItemPrototype implements Cloneable {

    public static Integer DEFAULT_ORDER = 0;
    public static final String DEFAULT_NAME = "?";
    public static final String DEFAULT_TYPE = "?";//

    Integer order;
    final StringProperty name;
    final StringProperty type;//
    Node node;
    double oldx, oldy;
    boolean isBold; 
    boolean isItalic; 
    
    String url = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    
    public boolean isIsBold() {
        return isBold;
    }

    public void setIsBold(boolean isBold) {
        this.isBold = isBold;
    }

    public boolean isIsItalic() {
        return isItalic;
    }

    public void setIsItalic(boolean isItalic) {
        this.isItalic = isItalic;
    }

    public double getOldx() {
        return oldx;
    }

    public void setOldx(double oldx) {
        this.oldx = oldx;
    }

    public double getOldy() {
        return oldy;
    }

    public void setOldy(double oldy) {
        this.oldy = oldy;
    }
    
    

    public LoGoItemPrototype() {
        order = 0;
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);

    }

    public LoGoItemPrototype(int initOrder, String initName,String initType, Node initNode) {
        this();//?
        order = initOrder;
        name.set(initName);
        type.set(initType);
        node =  initNode;
        node.setUserData(this);
    }
    
    public LoGoItemPrototype(int initOrder, String initName,String initType, Node initNode,String path) {
        this();//?
        order = initOrder;
        name.set(initName);
        type.set(initType);
        node =  initNode;
        this.url = path;
        node.setUserData(this);
    }

    public String getName() {
        return name.get();
    }

    public Node getNode() {
        return node;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

 
    public void setNode(Node node) {
        this.node = node;
    }

    
    public String getType() {
        return type.get();
    }

    public void setName(String value) {
        name.set(value);
    }

    public StringProperty nameProperty() {
        return name;
    }
    
   
    public void setType(String value) {
        type.set(value);
    }

    public StringProperty typeProperty() {
        return type;
    }

    
    public void reset() {
        
        setOrder(DEFAULT_ORDER);
        setName(DEFAULT_NAME);
        setType(DEFAULT_TYPE);

    }

    public Object clone() {
        if(((LoGoItemPrototype)node.getUserData()).getType().equals("Text")){
            //System.out.println("clone completed!!!!\n");
           Text text = new Text();
           text.setText(((Text)node).getText());
           text.setX(((Text)node).getX());
           text.setY(((Text)node).getY());
           text.setTextAlignment(TextAlignment.valueOf(((Text)node).getTextAlignment().toString()));
           text.setTextOrigin(VPos.valueOf(((Text)node).getTextOrigin().toString()));
           text.setBoundsType(TextBoundsType.valueOf(((Text)node).getBoundsType().toString()));
           text.setFill(Paint.valueOf(((Text) node).getFill().toString()));
           String familyname = ((Text) node).getFont().getFamily().toString();
           double size = ((Text) node).getFont().getSize();
           Font f; 
           FontWeight weight; 
           FontPosture pos; 
           if(this.isBold){
                weight = FontWeight.BOLD;
            }
            else{
                weight = FontWeight.NORMAL;
            }
            if(this.isItalic){
                pos = FontPosture.ITALIC;
            }
            else{
                pos = FontPosture.REGULAR;
            }
           f = Font.font(familyname, weight, pos, size);
            text.setFont(f);
            LoGoItemPrototype newTemp = new LoGoItemPrototype(-1, "Text", "Text", (Node)text);
           return newTemp;
        }
        else if(((LoGoItemPrototype)node.getUserData()).getType().equals("Rectangle")){
            RadialGradient r;
            Rectangle rec = new Rectangle();
            
            rec.setX(((Rectangle)node).getX());
            rec.setY(((Rectangle)node).getY());
            rec.setWidth(((Rectangle)node).getWidth());
            rec.setHeight(((Rectangle)node).getHeight());
            rec.setArcHeight(((Rectangle)node).getArcHeight());
            rec.setArcWidth(((Rectangle)node).getArcWidth());
            rec.setStrokeWidth(((Rectangle)node).getStrokeWidth());
            rec.setStroke(Paint.valueOf(((Rectangle)node).getStroke().toString()));
            
            r = RadialGradient.valueOf(((RadialGradient)((Rectangle)node).getFill()).toString());
            
            rec.setFill(r);
        
            LoGoItemPrototype newTemp = new LoGoItemPrototype(1,"Rectangle","Rectangle",rec);
            return newTemp;
        }
        
        else if(((LoGoItemPrototype)node.getUserData()).getType().equals("Rectangle")){
            RadialGradient r;
            Circle rec = new Circle();
            rec.setCenterX(((Circle)node).getCenterX());
            rec.setCenterY(((Circle)node).getCenterY());
            rec.setRadius(((Circle)node).getRadius());
            rec.setStrokeWidth(((Circle)node).getStrokeWidth());
            rec.setStroke(Paint.valueOf(((Circle)node).getStroke().toString()));
            
            r = RadialGradient.valueOf(((RadialGradient)((Circle)node).getFill()).toString());
            
            rec.setFill(r);
            LoGoItemPrototype newTemp = new LoGoItemPrototype(1,"Circle","Cirlce",rec);
            return newTemp;
        }
        else if(((LoGoItemPrototype)node.getUserData()).getType().equals("Image")){
            
            Image image = new Image(this.getUrl());
            ImageView iv = new ImageView(image);
            LoGoItemPrototype newTemp = new LoGoItemPrototype(1,"Image","Image",iv,this.getUrl());
            return newTemp;
        }
        else{
            LoGoItemPrototype newTemp = new LoGoItemPrototype();
            return newTemp;
        }

    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    

}
