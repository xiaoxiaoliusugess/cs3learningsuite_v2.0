/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import gologolo.GoLoGoLoApp;
import static gologolo.LoGoItemPropertyType.GLGL_BTL_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_B_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_EDIT_ITEM_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_FCYCLE_METHOD_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_BORDER_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_BORDER_RADIUS_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_BORDER_THICKNESS_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_CENTERX_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_CENTERY_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_DISTANCE_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_FOCUS_ANGLE_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_RADIUSE_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_STEP0_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_STEP1_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_TEXT_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_LTB_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_MOVEDOWN_ITEM_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_MOVEUP_ITEM_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_SLOPE_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_TEXTSIZE_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_TEXTTYPE_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_TRIANGLE_BUTTON;
import gologolo.data.LoGoData;


/**
 *
 * @author liuxiao
 */
public class LoGoSelectionFoolproofDesign implements FoolproofDesign{
 
    GoLoGoLoApp app;
    
    public LoGoSelectionFoolproofDesign(GoLoGoLoApp app){
        this.app = app;
    }
    
    @Override
    public void updateControls() {
        AppGUIModule gui = app.getGUIModule();
        
        LoGoData data = (LoGoData)app.getDataComponent();
        gui.getGUINode(GLGL_TRIANGLE_BUTTON).setDisable(true);
        boolean itemIsSelected = data.isItemSelected();
        gui.getGUINode(GLGL_MOVEUP_ITEM_BUTTON).setDisable(!(itemIsSelected) 
                || data.getItemIndex(data.getSelectedItem())== 0);

        gui.getGUINode(GLGL_MOVEDOWN_ITEM_BUTTON).setDisable(!(itemIsSelected) || 
                data.getItemIndex(data.getSelectedItem())==data.getNumItems()-1);
        
        gui.getGUINode(GLGL_EDIT_ITEM_BUTTON).setDisable(!itemIsSelected);

        if(!itemIsSelected ){
            gui.getGUINode(GLGL_B_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_SLOPE_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_BTL_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_LTB_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_TEXTTYPE_COMBOBOX).setDisable(true);
                gui.getGUINode(GLGL_TEXTSIZE_COMBOBOX).setDisable(true);
                gui.getGUINode(GLGL_FOR_TEXT_COLOR_PICKER).setDisable(true);
                gui.getGUINode(GLGL_FOR_BORDER_THICKNESS_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_BORDER_COLOR_PICKER).setDisable(false);
                gui.getGUINode(GLGL_FOR_BORDER_RADIUS_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_FOCUS_ANGLE_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_DISTANCE_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_CENTERX_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_CENTERY_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_RADIUSE_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FCYCLE_METHOD_COMBOBOX).setDisable(false);
                gui.getGUINode(GLGL_FOR_STEP0_COLOR_PICKER).setDisable(false);
                gui.getGUINode(GLGL_FOR_STEP1_COLOR_PICKER).setDisable(false);
         }
        else if(itemIsSelected){
            if(data.getSelectedItem().getType().equals("Text")){
                gui.getGUINode(GLGL_B_BUTTON).setDisable(false);
                gui.getGUINode(GLGL_SLOPE_BUTTON).setDisable(false);
                gui.getGUINode(GLGL_BTL_BUTTON).setDisable(false);
                gui.getGUINode(GLGL_LTB_BUTTON).setDisable(false);
                gui.getGUINode(GLGL_TEXTTYPE_COMBOBOX).setDisable(false);
                gui.getGUINode(GLGL_TEXTSIZE_COMBOBOX).setDisable(false);
                gui.getGUINode(GLGL_FOR_TEXT_COLOR_PICKER).setDisable(false);
                gui.getGUINode(GLGL_FOR_BORDER_THICKNESS_SILDER).setDisable(true);
                gui.getGUINode(GLGL_FOR_BORDER_COLOR_PICKER).setDisable(true);
                gui.getGUINode(GLGL_FOR_BORDER_RADIUS_SILDER).setDisable(true);
                gui.getGUINode(GLGL_FOR_FOCUS_ANGLE_SILDER).setDisable(true);
                gui.getGUINode(GLGL_FOR_DISTANCE_SILDER).setDisable(true);
                gui.getGUINode(GLGL_FOR_CENTERX_SILDER).setDisable(true);
                gui.getGUINode(GLGL_FOR_CENTERY_SILDER).setDisable(true);
                gui.getGUINode(GLGL_FOR_RADIUSE_SILDER).setDisable(true);
                gui.getGUINode(GLGL_FCYCLE_METHOD_COMBOBOX).setDisable(true);
                gui.getGUINode(GLGL_FOR_STEP0_COLOR_PICKER).setDisable(true);
                gui.getGUINode(GLGL_FOR_STEP1_COLOR_PICKER).setDisable(true);
            }
            else if(!data.getSelectedItem().getType().equals("Text") || !data.getSelectedItem().getType().equals("Image")){
                gui.getGUINode(GLGL_B_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_SLOPE_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_BTL_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_LTB_BUTTON).setDisable(true);
                gui.getGUINode(GLGL_TEXTTYPE_COMBOBOX).setDisable(true);
                gui.getGUINode(GLGL_TEXTSIZE_COMBOBOX).setDisable(true);
                gui.getGUINode(GLGL_FOR_TEXT_COLOR_PICKER).setDisable(true);
                gui.getGUINode(GLGL_FOR_BORDER_THICKNESS_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_BORDER_COLOR_PICKER).setDisable(false);
                gui.getGUINode(GLGL_FOR_BORDER_RADIUS_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_FOCUS_ANGLE_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_DISTANCE_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_CENTERX_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_CENTERY_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FOR_RADIUSE_SILDER).setDisable(false);
                gui.getGUINode(GLGL_FCYCLE_METHOD_COMBOBOX).setDisable(false);
                gui.getGUINode(GLGL_FOR_STEP0_COLOR_PICKER).setDisable(false);
                gui.getGUINode(GLGL_FOR_STEP1_COLOR_PICKER).setDisable(false);
        }
    }
        
        
        
        
        
       
    }
    
}
