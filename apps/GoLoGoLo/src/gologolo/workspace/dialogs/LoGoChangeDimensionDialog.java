/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialogs;

import static djf.AppPropertyType.GLGL_ITEM_DIALOG_ADD_HEADER_TEXT_INDJF;
import static djf.AppPropertyType.GLGL_ITEM_DIALOG_CANCEL_BUTTON_INDJF;
import static djf.AppPropertyType.GLGL_ITEM_DIALOG_HEIGHT_PROMPT;
import static djf.AppPropertyType.GLGL_ITEM_DIALOG_OK_BUTTON_INDJF;
import static djf.AppPropertyType.GLGL_ITEM_DIALOG_WEIGHT_PROMPT;
import djf.AppTemplate;
import djf.modules.AppLanguageModule;
import static djf.ui.style.DJFStyle.CLASS_GLGL_DIALOG_BUTTON;
import static djf.ui.style.DJFStyle.CLASS_GLGL_DIALOG_GRID;
import static djf.ui.style.DJFStyle.CLASS_GLGL_DIALOG_PANE;
import static djf.ui.style.DJFStyle.CLASS_GLGL_DIALOG_PROMPT;
import static djf.ui.style.DJFStyle.CLASS_GLGL_DIALOG_TEXT_FIELD;
import gologolo.data.LoGoData;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author liuxiao
 */
public class LoGoChangeDimensionDialog extends Stage{
    AppTemplate app;
    GridPane gridPane;
    
    Label headerLabel = new Label();
    Label heightLabel = new Label();
    TextField heightTextField = new TextField();
    Label weightLabel = new Label();
    TextField weightTextField = new TextField();
    
    HBox okCancelPane = new HBox();
    Button okButton = new Button();
    Button cancelButton = new Button();
    
    double changedHeight = -1;
    double changedWidth = -1;
    Boolean cancelOrNot = false; 

    public Boolean getCancelOrNot() {
        return cancelOrNot;
    }

    public void setCancelOrNot(Boolean cancelOrNot) {
        this.cancelOrNot = cancelOrNot;
    }

    public double getChangedHeight() {
        return changedHeight;
    }

    public double getChangedWidth() {
        return changedWidth;
    }
    
    public LoGoChangeDimensionDialog(AppTemplate initApp) {
        app = initApp;
        gridPane = new GridPane();
        
        gridPane.getStyleClass().add(CLASS_GLGL_DIALOG_GRID);
        
        initDialog();
        
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
//        scene.getStylesheets().add(CLASS_TDLM_DIALOG_GRID);                             
        
        // MAKE IT MODAL
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
    
    private void initDialog() {
        initGridNode(heightLabel,         GLGL_ITEM_DIALOG_HEIGHT_PROMPT,       CLASS_GLGL_DIALOG_PROMPT,       0, 1, 1, 1, true);
        initGridNode(heightTextField,     null,                                   CLASS_GLGL_DIALOG_TEXT_FIELD,   1, 1, 1, 1, false);
        
        initGridNode(weightLabel,         GLGL_ITEM_DIALOG_WEIGHT_PROMPT,    CLASS_GLGL_DIALOG_PROMPT,       0, 2, 1, 1, true);
        initGridNode(weightTextField,     null,                                   CLASS_GLGL_DIALOG_TEXT_FIELD,   1, 2, 1, 1, false);
        initGridNode(okCancelPane,        null,                                   CLASS_GLGL_DIALOG_PANE,         0, 3, 1, 1, false);

        okButton = new Button();
        cancelButton = new Button();
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_OK_BUTTON_INDJF, okButton);
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_CANCEL_BUTTON_INDJF, cancelButton);
        okButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);
        
        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_OK_BUTTON_INDJF + "_TEXT",    okButton.textProperty());
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_CANCEL_BUTTON_INDJF + "_TEXT",    cancelButton.textProperty());
        
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        
        cancelButton.setOnAction(e->{
            cancelOrNot = true;
            this.hide();
        }); 
        heightTextField.setOnAction(e->{
            processCompleteWork();
        });
        weightTextField.setOnAction(e->{
            processCompleteWork();
        });
    }
    
    private void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }
    
    public void showChangeDialog() {        
        // USE THE TEXT IN THE HEADER FOR ADD
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_ADD_HEADER_TEXT_INDJF);
        headerLabel.setText(headerText);
        setTitle(headerText);

        
        // USE THE TEXT IN THE HEADER FOR ADD
        heightTextField.setText(String.valueOf(((LoGoData)app.getDataComponent()).getBackground().getHeight()));
        weightTextField.setText(String.valueOf(((LoGoData)app.getDataComponent()).getBackground().getWidth()));
        // AND OPEN THE DIALOG
        showAndWait();
    }

    private void processCompleteWork() {
        changedHeight = Double.parseDouble(heightTextField.getText());
        changedWidth = Double.parseDouble(weightTextField.getText());
        this.hide();
    }
    
    
    

    
}
