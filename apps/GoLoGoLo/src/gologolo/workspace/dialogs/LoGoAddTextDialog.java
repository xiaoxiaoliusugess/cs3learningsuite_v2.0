/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialogs;



import djf.modules.AppLanguageModule;
import gologolo.GoLoGoLoApp;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_CANCEL_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_OK_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_ADD_HEADER_TEXT;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_CANCEL_BUTTON1;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_EDIT_HEADER_TEXT;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_HEADER;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_OK_BUTTON1;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_TEXT_PROMPT;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_BUTTON;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_GRID;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_HEADER;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_PANE;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_PROMPT;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_TEXT;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author liuxiao
 */
public class LoGoAddTextDialog extends Stage {
    GoLoGoLoApp app;
    GridPane gridPane;
    
    Label headerLabel = new Label();    
    Label textLabel = new Label();
    TextField textTextField = new TextField();  
    HBox okCancelPane = new HBox();
    Button okButton = new Button();
    Button cancelButton = new Button();
    
    LoGoItemPrototype itemToEdit;
    LoGoItemPrototype newItem;
    LoGoItemPrototype editItem;
    boolean editing;
    
   public LoGoAddTextDialog(GoLoGoLoApp initApp) {
        // KEEP THIS FOR WHEN THE WORK IS ENTERED
        app = initApp;

        // EVERYTHING GOES IN HERE
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_GLGL_DIALOG_GRID);
        initDialog();

        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
//        scene.getStylesheets().add(CLASS_TDLM_DIALOG_GRID);                             
        
        // MAKE IT MODAL
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
    
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }
    
    private void initDialog() {
        // THE NODES ABOVE GO DIRECTLY INSIDE THE GRID
        initGridNode(headerLabel,           GLGL_ITEM_DIALOG_HEADER,                CLASS_GLGL_DIALOG_HEADER,       0, 0, 3, 1, true);
        
        initGridNode(textLabel,         GLGL_ITEM_DIALOG_TEXT_PROMPT,       CLASS_GLGL_DIALOG_PROMPT,       0, 1, 1, 1, true);
        initGridNode(textTextField,     null,                                   CLASS_GLGL_DIALOG_TEXT,   1, 1, 1, 1, false);
             
        initGridNode(okCancelPane,          null,                                   CLASS_GLGL_DIALOG_PANE,         0, 3, 3, 1, false);

        okButton = new Button();
        cancelButton = new Button();
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_OK_BUTTON1, okButton);
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_CANCEL_BUTTON1, cancelButton);
        okButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);

        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_OK_BUTTON1 + "_TEXT",    okButton.textProperty());
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_CANCEL_BUTTON1 + "_TEXT",    cancelButton.textProperty());
       
        // AND SETUP THE EVENT HANDLERS
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
           newItem = null;
           editItem = null;
           editing = false;
           this.hide();
        });   
        
    }
    
    private void makeNewItem() {
        LoGoData data = (LoGoData)app.getDataComponent();
        //data.reSetOrder();
        int index = data.getNumItems();
        Integer order = index + 1;
        String name = "Text";
        String type = "Text";//
        String str = textTextField.getText();
        
        //has valid input
        if(str!=null)
        {
            Node node = new Text(str);
            newItem = new LoGoItemPrototype(order,name,type,node);
        }
        else{
            newItem = null;
        }
        
        
         //
        System.out.print(newItem.getNode().toString());
        this.hide();
    }
    
    private void makeEditItem() {
        LoGoData data = (LoGoData)app.getDataComponent();
        //data.reSetOrder();
        int index = data.getNumItems();
        Integer order = index + 1;
        String name = "Text";
        String type = "Text";//
        
        //edit is null, we should delete

        if(textTextField.getCharacters().length()==0)
        {
            Node node = new Text(textTextField.getText());
            editItem = new LoGoItemPrototype(-1, name, type,node);
        }
        else{
            Node node = new Text(textTextField.getText());
            editItem = new LoGoItemPrototype(order, name, type,node);
        }
        
         //
        this.hide();
    }
    
    
    private void processCompleteWork() {
        LoGoData data = (LoGoData)app.getDataComponent();
        if (editing) {
            this.makeEditItem();
        }
        // IF WE ARE ADDING
        else {
            this.makeNewItem();
        }
        
        // CLOSE THE DIALOG
        
        this.hide();
    }

    public boolean isEditing() {
        return editing;
    }
    
    public void showAddDialog() {        
        // USE THE TEXT IN THE HEADER FOR ADD
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_ADD_HEADER_TEXT);
        
        headerLabel.setText(headerText);
        setTitle(headerText);

        textTextField.setText("");
        
        // WE ARE ADDING A NEW ONE, NOT EDITING
        editing = false;
        editItem = null;
        
        // AND OPEN THE DIALOG
        showAndWait();
    }
    
    public void showEditDialog(LoGoItemPrototype initItemToEdit) {
        // WE'LL NEED THIS FOR VALIDATION
        itemToEdit = initItemToEdit;
        
        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_EDIT_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        
        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        editing = true;
        editItem = null;
        
        // USE THE TEXT IN THE textTextField FOR EDIT
        textTextField.setText(((Text)itemToEdit.getNode()).getText());
        
        // AND OPEN THE DIALOG
        showAndWait();
    }
    
    public LoGoItemPrototype getNewItem() {
        return newItem;
    }
    
     public LoGoItemPrototype getEditItem() {
        return editItem;
    }
}
