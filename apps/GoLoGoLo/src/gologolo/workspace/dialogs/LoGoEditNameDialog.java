/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialogs;

import static djf.AppPropertyType.NEW_VALIDDATA_CONTENT;
import static djf.AppPropertyType.NEW_VALIDDATA_TITLE;
import djf.modules.AppLanguageModule;
import gologolo.GoLoGoLoApp;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_CANCEL_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_OK_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_ADD_HEADER_TEXT;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_DIALOG_EDIT_HEADER_TEXT;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_EDIT_NAME_DIALOG_HEADER;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_EDIT_NAME_DIALOG_NAME_PROMPT;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_EDIT_NAME_DIALOG_TEXT_PROMPT;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_BUTTON;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_GRID;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_HEADER;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_PANE;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_PROMPT;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_DIALOG_TEXT;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author liuxiao
 */
public class LoGoEditNameDialog extends Stage{
    GoLoGoLoApp app;
    GridPane gridPane;
    
    Label headerLabel = new Label();    
    Label nameLabel = new Label();
    TextField nameTextField = new TextField();  
    HBox okCancelPane = new HBox();
    Button okButton = new Button();
    Button cancelButton = new Button();
    
    LoGoItemPrototype newItem;
    LoGoItemPrototype editItem;
    boolean editing;
    
    public LoGoEditNameDialog(GoLoGoLoApp initApp) {
        // KEEP THIS FOR WHEN THE WORK IS ENTERED
        app = initApp;

        // EVERYTHING GOES IN HERE
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_GLGL_DIALOG_GRID);
        initDialog();

        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
//        scene.getStylesheets().add(CLASS_TDLM_DIALOG_GRID);                             
        
        // MAKE IT MODAL
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
    
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }
    
    private void initDialog() {
        // THE NODES ABOVE GO DIRECTLY INSIDE THE GRID
        initGridNode(headerLabel,           GLGL_ITEM_EDIT_NAME_DIALOG_HEADER,                CLASS_GLGL_DIALOG_HEADER,       0, 0, 3, 1, true);
        
        initGridNode(nameLabel,         GLGL_ITEM_EDIT_NAME_DIALOG_NAME_PROMPT,       CLASS_GLGL_DIALOG_PROMPT,       0, 1, 1, 1, true);
        initGridNode(nameTextField,     null,                                   CLASS_GLGL_DIALOG_TEXT,   1, 1, 1, 1, false);
             
        initGridNode(okCancelPane,          null,                                   CLASS_GLGL_DIALOG_PANE,         0, 3, 3, 1, false);

        okButton = new Button();
        cancelButton = new Button();
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_OK_BUTTON, okButton);
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_CANCEL_BUTTON, cancelButton);
        okButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);

        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_OK_BUTTON + "_TEXT",    okButton.textProperty());
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_CANCEL_BUTTON + "_TEXT",    cancelButton.textProperty());
       
        // AND SETUP THE EVENT HANDLERS
        
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
           editItem = null;
           editing = false;
           this.hide();
        });   
        nameTextField.setOnAction(e->{
            processCompleteWork();
        });
    }
   
    private void processCompleteWork() {      
        editItem.setName(nameTextField.getText());
        
        // CLOSE THE DIALOG
        this.hide();
    }

    public boolean isEditing() {
        return editing;
    }
    

    
    public void showEditDialog(LoGoItemPrototype initItemToEdit) {
        // WE'LL NEED THIS FOR VALIDATION
        
        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_EDIT_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        
        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        editItem = initItemToEdit;
        
        // USE THE TEXT IN THE HEADER FOR EDIT
        nameTextField.setText(initItemToEdit.getName());
        nameTextField.selectAll();
        
        // AND OPEN THE DIALOG
        showAndWait();
    }
    
    public LoGoItemPrototype getNewItem() {
        return newItem;
    }
    
     public LoGoItemPrototype getEditItem() {
        return editItem;
    }
    
}
