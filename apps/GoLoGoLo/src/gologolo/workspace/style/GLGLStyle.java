/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.style;

/**
 *
 * @author liuxiao
 */
public class GLGLStyle {
    public static final String EMPTY_TEXT = "";
    public static final int BUTTON_TAG_WIDTH = 75;
    
    public static final String CLASS_GLGL_PANE          = "glgl_pane";
    public static final String CLASS_GLGL_BOX           = "glgl_box";            
    public static final String CLASS_GLGL_BIG_HEADER    = "glgl_big_header";
    public static final String CLASS_GLGL_SMALL_HEADER  = "glgl_small_header";
    public static final String CLASS_GLGL_PROMPT        = "glgl_prompt";
    public static final String CLASS_GLGL_TEXT_FIELD    = "glgl_text_field";
    public static final String CLASS_GLGL_BUTTON        = "glgl_button";
    public static final String CLASS_GLGL_TABLE         = "glgl_table";
    public static final String CLASS_GLGL_COLUMN        = "glgl_column";
    public static final String CLASS_GLGL_LABEL        = "glgl_label";
    public static final String CLASS_GLGL_LABEL_CG        = "glgl_label_cg";
    public static final String CLASS_GLGL_SLIDER       ="glgl_slider";
    public static final String CLASS_GLGL_COLOR_PICKER       ="glgl_color_picker";    
    public static final String CLASS_GLGL_BOX_RIGHT    ="glgl_box_right";
    public static final String CLASS_GLGL_CENTER_PANE ="glgl_center_pane";
    
    
    // STYLE CLASSES FOR THE ADD/EDIT ITEM DIALOG
    public static final String CLASS_GLGL_DIALOG_GRID           = "glgl_dialog_grid";
    public static final String CLASS_GLGL_DIALOG_HEADER         = "glgl_dialog_header";
    public static final String CLASS_GLGL_DIALOG_PROMPT         = "glgl_dialog_prompt";
    public static final String CLASS_GLGL_DIALOG_TEXT_FIELD     = "glgl_dialog_text_field";
    public static final String CLASS_GLGL_DIALOG_DATE_PICKER    = "glgl_dialog_date_picker";
    public static final String CLASS_GLGL_DIALOG_CHECK_BOX      = "glgl_dialog_check_box";
    public static final String CLASS_GLGL_DIALOG_BUTTON         = "glgl_dialog_button";
    public static final String CLASS_GLGL_DIALOG_PANE           = "glgl_dialog_pane";
    
    public static final String CLASS_GLGL_DIALOG_TEXT= "glgl_dialog_text";

}
