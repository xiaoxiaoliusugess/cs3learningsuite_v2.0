/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.controllers;

import gologolo.data.LoGoItemPrototype;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import jtps.jTPS_Transaction;

/**
 *
 * @author liuxiao
 */
public class ChangeRadiusTickness_Transaction implements jTPS_Transaction{
    LoGoItemPrototype item;
    double arcHeight;
    double arcWeight;
    double oriArcHeight;
    double oriArcWeight;

    public ChangeRadiusTickness_Transaction(LoGoItemPrototype item, double arcHeight, double arcWeight) {
        this.item = item;
        this.arcHeight = arcHeight;
        this.arcWeight = arcWeight;
        if(item.getType().equals("Rectangle")){
            oriArcHeight = ((Rectangle)item.getNode()).getArcHeight();
            oriArcWeight = ((Rectangle)item.getNode()).getArcWidth();
        }
        
    }

    @Override
    public void doTransaction() {
        if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setArcHeight(arcHeight);
            ((Rectangle)item.getNode()).setArcWidth(arcWeight);
        }
    }

    @Override
    public void undoTransaction() {
        if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setArcHeight(oriArcHeight);
            ((Rectangle)item.getNode()).setArcWidth(oriArcWeight);
        }
    }
}
    

