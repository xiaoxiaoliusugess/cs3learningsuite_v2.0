/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.controllers;


import gologolo.transactions.ChangeColorRadius_Transaction;
import gologolo.transactions.ChangeBorderColor_Transaction;
import gologolo.transactions.ChangeBorderTickness_Transaction;
import gologolo.transactions.ChangeTextColor_Transaction;
import gologolo.transactions.ChangedFont_Transaction;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.LOAD_WORK_TITLE;
import djf.ui.dialogs.AppDialogsFacade;
import gologolo.GoLoGoLoApp;

import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import gologolo.transactions.AddImage_Transaction;
import gologolo.transactions.AddRec_Transaction;
import gologolo.transactions.AddText_Transaction;
import gologolo.transactions.ChangeBB_Transaction;
import gologolo.transactions.Delete_Transaction;
import gologolo.transactions.EditItem_Transaction;
import gologolo.transactions.EditName_Transaction;
import gologolo.transactions.MoveDown_Transaction;
import gologolo.transactions.MoveUpItems_Transaction;
import gologolo.transactions.ResizeCircle_Transaction;
import gologolo.transactions.ResizeLogo_transaction;
import gologolo.transactions.grapDrap_Transaction;
import gologolo.workspace.LoGoWorkspace;
import gologolo.workspace.dialogs.LoGoAddTextDialog;
import gologolo.workspace.dialogs.LoGoChangeDimensionDialog;
import gologolo.workspace.dialogs.LoGoEditNameDialog;
import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

import javafx.scene.text.Text;
import properties_manager.PropertiesManager;


/**
 *
 * @author liuxiao
 */
public class ItemsController {
    GoLoGoLoApp app;
    LoGoAddTextDialog addTextDialog;
    LoGoEditNameDialog editNameDialog;
    LoGoChangeDimensionDialog dialog;
    
     static double orgSceneX, orgSceneY;
     static double orgTranslateX, orgTranslateY;
     static double newTranslateX,newTranslateY;
     static boolean moved;
     boolean selected = false;
     Node node;
     static double radius;
    
    public ItemsController(GoLoGoLoApp initApp) {
        app = initApp;
        
        addTextDialog = new LoGoAddTextDialog(app);
        editNameDialog = new LoGoEditNameDialog(app);
        dialog = new LoGoChangeDimensionDialog(app);
    }
    
    public static void checkMoved(double x,double y){
        
        if(x!=0 || y!=0){
            moved = true;
        }
        else
           moved = false;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    
    public void processAddText() {
        addTextDialog.showAddDialog();
        
        LoGoItemPrototype newItem = addTextDialog.getNewItem();  
        
        if (newItem != null) {
            LoGoData data = (LoGoData)app.getDataComponent();
            Text text = (Text)newItem.getNode();
            AddText_Transaction transaction = new AddText_Transaction(app, data, newItem);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
            int index = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().indexOf(text);
            text.setFont(Font.font("Arial"));
            processgrapDp(text,newItem,index);
            enableNodeHighlight(data,text,newItem);
            
        }    
    }
    
    public void processAddCircle() {
        
        Circle circle = new Circle(0,0,30);
        circle.setFill(new RadialGradient(0,0,0,0,0,true,CycleMethod.NO_CYCLE, new Stop[] {new Stop(0, Color.DARKGRAY), new Stop(1, Color.DARKGRAY)}));
        circle.setStroke(Color.BLACK);
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype newItem = new LoGoItemPrototype(0,"Circle","Circle",circle);
        
        DropShadow ds2 = new DropShadow();
        ds2.setColor(Color.CORAL);
        circle.setEffect(ds2);  
        data.clearSelected();
        data.selectItem(newItem);
        
        AddRec_Transaction transaction = new AddRec_Transaction(app,data,newItem);
        app.processTransaction(transaction);
        app.getFileModule().markAsEdited(true);
        int index1 = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().indexOf(circle);
        
        processgrapDp(circle,newItem,index1);
        enableNodeHighlight(data,circle,newItem);
        resizeCircleBScollM();
        
        }
    
    public void processAddRec() {
      Rectangle rec = new Rectangle(100,60);
        rec.setFill(new RadialGradient(0,0,0,0,0,true,CycleMethod.NO_CYCLE, new Stop[] {new Stop(0, Color.DARKGRAY), new Stop(1, Color.DARKGRAY)}));
        rec.setStroke(Color.BLACK);
        
        LoGoData data = (LoGoData)app.getDataComponent();    
        int index = data.getNumItems();
        int order = index + 1;
        LoGoItemPrototype newItem = new LoGoItemPrototype(order,"Rectangle","Rectangle",rec);
        
        DropShadow ds2 = new DropShadow();
        ds2.setColor(Color.CORAL);
        rec.setEffect(ds2);  
        data.clearSelected();
        data.selectItem(newItem);
        
        AddRec_Transaction transaction = new AddRec_Transaction(app,data,newItem);
        app.processTransaction(transaction);
        app.getFileModule().markAsEdited(true);
        int index1 = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().indexOf(rec);
        
        processgrapDp(rec,newItem,index1);
        enableNodeHighlight(data,rec,newItem);
    }
    
    public void enableNodeHighlight(LoGoData data,Node node,LoGoItemPrototype newItem)
    {
        node.setOnMouseClicked((MouseEvent e) -> {
            
            if (e.getClickCount() == 2 && newItem.getType().equals("Text")) {
                
                
                addTextDialog.showEditDialog(newItem);
                LoGoItemPrototype editItem = addTextDialog.getEditItem(); 
                
                //编辑成功
                
                
                //this means edit text is null, we should delete it
                if(editItem!=null){
                    if(editItem.getOrder() == -1)
                    {
                        //process delete transc=action
                        int index = data.getNodeIndexInPane(newItem);
                        Delete_Transaction transaction = new Delete_Transaction(app,data, newItem, index);
                        app.processTransaction(transaction);
                        app.getFileModule().markAsEdited(true);

                    }
                    else if(editItem!=null){
                        EditItem_Transaction t = new EditItem_Transaction(data,newItem,((Text)editItem.getNode()).getText());
                        app.processTransaction(t);
                        app.getFileModule().markAsEdited(true);
                    }

                    //edit text is null, this is we click cancel button
                    else{

                    }
                }
            }
            
            DropShadow ds2 = new DropShadow();
            ds2.setColor(Color.CORAL);
            node.setEffect(ds2);  
            data.clearSelected();
            data.selectItem(newItem);
        });
    }
    
    
    public void processgrapDp(Node node,LoGoItemPrototype type, int index){
        
        node.setOnMousePressed((MouseEvent e) -> {
                   
            ItemsController.orgSceneX = e.getSceneX();
            ItemsController.orgSceneY = e.getSceneY();
            if(type.getType().equals("Rectangle")){
                  orgTranslateX = ((Rectangle)(e.getSource())).getTranslateX();
                  orgTranslateY = ((Rectangle)(e.getSource())).getTranslateY();
            }
            if(type.getType().equals("Text")){
                 orgTranslateX = ((Text)(e.getSource())).getTranslateX();
                 orgTranslateY = ((Text)(e.getSource())).getTranslateY();
            }
            if(type.getType().equals("Image")){
                 orgTranslateX = ((ImageView)(e.getSource())).getTranslateX();
                 orgTranslateY = ((ImageView)(e.getSource())).getTranslateY();
            }
            if(type.getType().equals("Circle")){
                 orgTranslateX = ((Circle)(e.getSource())).getTranslateX();
                 orgTranslateY = ((Circle)(e.getSource())).getTranslateY();
            }
        });
            
            
        node.setOnMouseDragged((MouseEvent e) -> {
            double offsetX = e.getSceneX() - orgSceneX;
            double offsetY = e.getSceneY() - orgSceneY;
            
            ItemsController.newTranslateX = orgTranslateX + offsetX/node.getParent().getScaleX();
            ItemsController.newTranslateY = orgTranslateY + offsetY/node.getParent().getScaleY();

            if(type.getType().equals("Rectangle")){
                ((Rectangle)(e.getSource())).setTranslateX(newTranslateX);
                ((Rectangle)(e.getSource())).setTranslateY(newTranslateY);
            }
            if(type.getType().equals("Text")){
                 ((Text)(e.getSource())).setTranslateX(newTranslateX);
                 ((Text)(e.getSource())).setTranslateY(newTranslateY);
            }
            if(type.getType().equals("Image")){
                 ((ImageView)(e.getSource())).setTranslateX(newTranslateX);
                 ((ImageView)(e.getSource())).setTranslateY(newTranslateY);
            }
            if(type.getType().equals("Circle")){
                 ((Circle)(e.getSource())).setTranslateX(newTranslateX);
                 ((Circle)(e.getSource())).setTranslateY(newTranslateY);
            }

        });
            
        node.setOnMouseReleased((MouseEvent e) -> 
        {
            checkMoved(newTranslateX,newTranslateY);
            if(moved)
            {
                grapDrap_Transaction transaction3 = new grapDrap_Transaction(app,type,ItemsController.orgTranslateX,ItemsController.orgTranslateY,ItemsController.newTranslateX,ItemsController.newTranslateY,index);
                app.processTransaction(transaction3);
                app.getFileModule().markAsEdited(true);
            }

            orgSceneX = 0;
            orgSceneY = 0;
            orgTranslateX = 0;
            orgTranslateY = 0;
            newTranslateX = 0;
            newTranslateY = 0;
        });
            
                
    }
    
    public void cancelHight(){
            LoGoData data = (LoGoData)app.getDataComponent();
           if(data.getNumItems()!= 0){
                for(int i = 0; i < data.getNumItems();i++){
                    data.getItems().get(i).getNode().setEffect(null);
                }
                
                DropShadow ds2 = new DropShadow();
                           ds2.setColor(Color.CORAL);
                data.getSelectedItem().getNode().setEffect(ds2);
           }
           else{  
           }  
        }
    
    
    
   
    public void processDelete(){
        LoGoData data = (LoGoData)app.getDataComponent();
        
        if(data.isItemSelected())
        {
            LoGoItemPrototype select = data.getSelectedItem();
            int index = data.getItems().indexOf(select);
            Node node = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().get(index);
            Delete_Transaction transaction = new Delete_Transaction(app,data,select,index);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
        }
    }

    public void processMoveUp() {
        LoGoData data = (LoGoData)app.getDataComponent();
        
        if(data.isItemSelected()){
            int i = data.getItemIndex(data.getSelectedItem());
            
            if(i!=0){
               LoGoItemPrototype itemToMove = data.getSelectedItem();
               
               MoveUpItems_Transaction transaction = new MoveUpItems_Transaction(app,data,itemToMove,i);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
                app.getFoolproofModule().updateAll();
                app.getFileModule().markAsEdited(true);
                
            }
        }
    }

    public void processMoveDown() {
        LoGoData data = (LoGoData)app.getDataComponent();
         if (data.isItemSelected()){
            int i = data.getItemIndex(data.getSelectedItem());
            if(data.getItemIndex(data.getSelectedItem())!= data.getItems().size()-1){
                LoGoItemPrototype itemsToPaste = data.getSelectedItem();
                MoveDown_Transaction transaction = new MoveDown_Transaction(data,itemsToPaste,i);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
                app.getFoolproofModule().updateAll();
                app.getFileModule().markAsEdited(true);
                
            }
        }
    }

    public void processEditName() {
        
        LoGoData data = (LoGoData)app.getDataComponent();
        if (data.isItemSelected()){
            
            String oriName = data.getSelectedItem().getName();
            editNameDialog.showEditDialog(data.getSelectedItem());
            
            
            if(editNameDialog.getEditItem()!=null)
            {
                LoGoItemPrototype editItem = editNameDialog.getEditItem(); 
                String changedName = editItem.getName();
                EditName_Transaction transacetion = new EditName_Transaction(data,editItem,oriName,changedName);
                app.processTransaction(transacetion);
                app.getFileModule().markAsEdited(true);
                app.getFoolproofModule().updateAll();
            }
            
            //user clicke cancel
            else{}
        }
        
    }
    public void setNodeHighlight() {
        
        LoGoData data = (LoGoData)app.getDataComponent();
        
        data.highLightCanvasBtable();
    }
    
    public void processAddImage() {
        LoGoData data = (LoGoData)app.getDataComponent();
        ImageView image;
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ImageView iv = null;
        String path = null;
        Image image1;
	
        // AND NOW ASK THE USER FOR THE FILE TO OPEN
        File selectedFile = AppDialogsFacade.showOpenDialog(app.getGUIModule().getWindow(), LOAD_WORK_TITLE);

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                //save the image as an gologoprototype  and process add image transaction (add in table and add in pane)
                path = "file:"+selectedFile.getPath();
                image1 = new Image(path);
                iv = new ImageView(image1);
            } catch (Exception e) {
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), LOAD_ERROR_TITLE, LOAD_ERROR_CONTENT);
            }
            image = iv;
            
        if(image!=null){
            LoGoItemPrototype newItem = new LoGoItemPrototype(1,"Image","Image",image,path);
            
            AddImage_Transaction t = new AddImage_Transaction(app,data,newItem);
            app.processTransaction(t);
            app.getFileModule().markAsEdited(true);
            int index1 = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace()).getCenter()).getChildren().get(0)).getChildren().indexOf(image);
            processgrapDp(image,newItem,index1);
            enableNodeHighlight(data,image,newItem);
        }
        
        
        
    }
//    private ImageView promptToOpen() {
//        LoGoData data = (LoGoData)app.getDataComponent();
//	// WE'LL NEED TO GET CUSTOMIZED STUFF WITH THIS
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
//        ImageView iv = null;
//        String path;
//        Image image;
//	
//        // AND NOW ASK THE USER FOR THE FILE TO OPEN
//        File selectedFile = AppDialogsFacade.showOpenDialog(app.getGUIModule().getWindow(), LOAD_WORK_TITLE);
//
//        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
//        if (selectedFile != null) {
//            try {
//                //save the image as an gologoprototype  and process add image transaction (add in table and add in pane)
//                path = selectedFile.getPath();
//                image = new Image("file:" + path);
//                iv = new ImageView(image);
//            } catch (Exception e) {
//                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), LOAD_ERROR_TITLE, LOAD_ERROR_CONTENT);
//            }
//        }
//        
//        
//        
//        return iv;
    }

    public void processChangeDimension() {
        LoGoData data = (LoGoData)app.getDataComponent();
        dialog.showChangeDialog();
        if(dialog.getCancelOrNot()==false){
            double actualHeight = data.getBackground().getMaxHeight();
            double actualWidth =  data.getBackground().getMaxWidth();

            double height = dialog.getChangedHeight();
            double width = dialog.getChangedWidth();

            ResizeLogo_transaction transacetion = new ResizeLogo_transaction(data,actualHeight,actualWidth,height,width);
            app.processTransaction(transacetion);
            app.getFileModule().markAsEdited(true);
        }
        //app.getFoolproofModule().updateAll();
    }

    public void processAllcomponentDouble() {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleX(2 * ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleX());
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleY(2 * ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleY());
        
    }

    public void processAllcomponentHalf() {
        
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleX(((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleX()/2);
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleY(((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleY()/2);
        
    }

    public void processAllNormalSize() {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleX(1);
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleY(1);
       ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setTranslateX(0);
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setTranslateY(0);
    }

    public void resizeCircleBScollM(){
        
        LoGoData data = (LoGoData)app.getDataComponent();
        Circle temp = ((Circle)(data.getSelectedItem().getNode()));
        
        DoubleProperty r = new SimpleDoubleProperty();
        
        temp.setOnScrollStarted(e->{
            r.set(temp.getRadius());
        });
        
        temp.setOnScroll(e-> {
            double zoomFactor = 1.05;
            double deltaY = e.getDeltaY();
            if (deltaY < 0){
                zoomFactor = 2.0 - zoomFactor;
            }
        temp.setRadius(temp.getRadius() * zoomFactor);
        temp.setRadius(temp.getRadius() * zoomFactor);
        });
        
        temp.setOnScrollFinished(e1->{
            ResizeCircle_Transaction transacetion = new ResizeCircle_Transaction(data.getSelectedItem(),r.get());
            app.processTransaction(transacetion);
            app.getFileModule().markAsEdited(true);
        });
    }

    public void processTextFont(String familyname, int size,boolean changedBold,boolean changedItalic) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        
        Font f; 
        FontWeight weight; 
        FontPosture pos; 
        Text node = (Text)item.getNode();
        
        if(item.getType().equals("Text"))
        {
            if(item.isIsBold()) {
                if(changedBold)
                    weight = FontWeight.NORMAL;
                else
                    weight = FontWeight.BOLD;
            }
                
            else {
                if(changedBold)
                    weight = FontWeight.BOLD;
                else
                    weight = FontWeight.NORMAL;
            }
            if(item.isIsItalic()) {
                if(changedItalic)
                    pos = FontPosture.REGULAR;
                else
                    pos = FontPosture.ITALIC;
            }
                
            else {
                if(changedItalic)
                    pos = FontPosture.ITALIC;
                else
                    pos = FontPosture.REGULAR;
            }
            
            f = Font.font(familyname, weight, pos, size);
            ChangedFont_Transaction transacetion = new ChangedFont_Transaction(item,f,changedBold,changedItalic);
            app.processTransaction(transacetion);
            app.getFileModule().markAsEdited(true);
           
        }
    }

    public void changeTextColor(Color color) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        
        ChangeTextColor_Transaction transacetion = new ChangeTextColor_Transaction(item,color);
        app.processTransaction(transacetion);
        app.getFileModule().markAsEdited(true);
    }

    public void changeBorderThickness(double value) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        
        if(!item.getType().equals("Text")||item.equals(null))
        {
            ChangeBorderTickness_Transaction transacetion = new ChangeBorderTickness_Transaction(item,value);
            app.processTransaction(transacetion);
            app.getFileModule().markAsEdited(true);
            
        }
    }

    public void changeBorderColor(Color color) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        
        ChangeBorderColor_Transaction transacetion = new ChangeBorderColor_Transaction(item,color);
        app.processTransaction(transacetion);
        app.getFileModule().markAsEdited(true);
        
    }

    public void changeBorderThickness2(double value) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        
        if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setStrokeWidth(value);
        }
        else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setStrokeWidth(value);
        }
        
    }

    public void changeRadiusThickness(double value) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        double arcHeight =  value;
        double arcWeight = value;
        
        if(item.getType().equals("Rectangle"))
        {
            ChangeRadiusTickness_Transaction transacetion = new ChangeRadiusTickness_Transaction(item,arcHeight,arcWeight);
            app.processTransaction(transacetion);
            app.getFileModule().markAsEdited(true);
        }
        
    }

    public void changeRadiusThickness1(double value) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        
        if(item.getType().equals("Rectangle"))
        {
            ((Rectangle)item.getNode()).setArcHeight(value);
            ((Rectangle)item.getNode()).setArcWidth(value);
        }
    }

    public void changeColorRadius(RadialGradient componentFRG) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        StackPane pane = data.getBackground();
       
        if(!data.isItemSelected()){
            
            ChangeBB_Transaction transaction = new ChangeBB_Transaction(pane, componentFRG);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
            
        }
        else{
            ChangeColorRadius_Transaction transacetion = new ChangeColorRadius_Transaction(item,componentFRG);
            app.processTransaction(transacetion);
            app.getFileModule().markAsEdited(true);
        }
        
        
        
    }

    public void changeColorRadius1(RadialGradient componentFRG) {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype item = data.getSelectedItem();
        
        if(!data.isItemSelected()){
            data.getBackground().setBackground(new Background(new BackgroundFill(componentFRG,null,null)));
            
        }
        else{
            if(item.getType().equals("Rectangle")){
            ((Rectangle)item.getNode()).setFill(componentFRG);
            
            }
            else if(item.getType().equals("Circle")){
            ((Circle)item.getNode()).setFill(componentFRG);
            }
            
        }
        
    }

    public void enlargeCanvas() {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleX(1.2 * ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleX());
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleY(1.2 * ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleY());
    }

    public void smallCanvas() {
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleX(0.5 * ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleX());
        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).setScaleY(0.5 * ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
                .getCenter()).getChildren().get(0)).getScaleY());
    }

    
}
        
        
   

