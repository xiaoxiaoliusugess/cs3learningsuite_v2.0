/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.controllers;

import djf.AppTemplate;
import djf.modules.AppGUIModule;
import gologolo.GoLoGoLoApp;
import gologolo.LoGoItemPropertyType;
import static gologolo.LoGoItemPropertyType.GLGL_ITEMS_TABLE_VIEW;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;


/**
 *
 * @author liuxiao
 */
public class ItemsTableController {
    GoLoGoLoApp app;

    public ItemsTableController(AppTemplate initApp) {
        app = (GoLoGoLoApp)initApp;
    }

    public void processChangeTableSize() {
        AppGUIModule gui = app.getGUIModule();
        TableView<LoGoItemPropertyType> itemsTable = (TableView)gui.getGUINode(GLGL_ITEMS_TABLE_VIEW);
        ObservableList columns = itemsTable.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = (TableColumn)columns.get(i);
            column.setMinWidth(itemsTable.widthProperty().getValue()/columns.size());
            column.setMaxWidth(itemsTable.widthProperty().getValue()/columns.size());
        }
    }    
    
    
}