/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace;

import static djf.AppPropertyType.RESET_VIEW_PORT_BUTTON;
import static djf.AppPropertyType.RESIZE_LOGO_BUTTON;
import static djf.AppPropertyType.SNAP_CHECK_BOX;
import static djf.AppPropertyType.SNAP_LABEL;
import static djf.AppPropertyType.ZOOM_IN_BUTTON;
import static djf.AppPropertyType.ZOOM_OUT_BUTTON;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import static djf.ui.style.DJFStyle.CLASS_DJF_CHECK_BOX;
import static djf.ui.style.DJFStyle.CLASS_DJF_ICON_BUTTON;
import static djf.ui.style.DJFStyle.CLASS_SNAP_LABEL;
import gologolo.GoLoGoLoApp;
import static gologolo.LoGoItemPropertyType.GLGL_A_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_ADD_ITEM_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_BORDER_COLOR_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_BORDER_RADIUS_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_BORDER_THICKNESS_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_BTL_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_B_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_CENTERX_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_CENTERY_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_CENTER_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_CIRCLE_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_COLOR_GRADIENT_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_CYCLE_METHOD_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_DEFAULT_METHOD;
import static gologolo.LoGoItemPropertyType.GLGL_DEFAULT_SIZE;
import static gologolo.LoGoItemPropertyType.GLGL_DEFAULT_TYPE;
import static gologolo.LoGoItemPropertyType.GLGL_DELETE_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_EDIT_ITEM_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_FCYCLE_METHOD_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_FOCUS_ANGLE_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_FOCUS_DISTANCE_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_FOOLPROOF_SETTINGS;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_BORDER_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_BORDER_RADIUS_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_BORDER_THICKNESS_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_CENTERX_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_CENTERY_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_DISTANCE_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_FOCUS_ANGLE_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_RADIUSE_SILDER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_STEP0_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_STEP1_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_FOR_TEXT_COLOR_PICKER;
import static gologolo.LoGoItemPropertyType.GLGL_IMAGE_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_ITEMS_TABLE_VIEW;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_BUTTONS_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_ITEM_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_LTB_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_MOVEDOWN_ITEM_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_MOVEUP_ITEM_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_NAME_COLUMN;
import static gologolo.LoGoItemPropertyType.GLGL_OPTIONLISTSSIZE_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_ORDER_COLUMN;
import static gologolo.LoGoItemPropertyType.GLGL_REC_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_RIGHT_COLOR_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_RIGHT_COMPONENT_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_RIGHT_F_IMAGE_MAIN_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_RIGHT_IMAGE_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_RIGHT_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_SLOPE_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_TRIANGLE_BUTTON;
import static gologolo.LoGoItemPropertyType.GLGL_TYPE_COLUMN;
import static gologolo.LoGoItemPropertyType.GLGL_OPTIONLISTS_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_OPTIONLISTS_FCYCLE_METHOD_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_RADIUS_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_RIGHT_BORDER_PANE;
import static gologolo.LoGoItemPropertyType.GLGL_STEP0_COLOR_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_STEP1_COLOR_LABEL;
import static gologolo.LoGoItemPropertyType.GLGL_TEXTSIZE_COMBOBOX;
import static gologolo.LoGoItemPropertyType.GLGL_TEXTTYPE_COMBOBOX;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import gologolo.workspace.controllers.ItemsController;
import gologolo.workspace.foolproof.LoGoSelectionFoolproofDesign;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_BOX;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_BUTTON;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_CENTER_PANE;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_COLOR_PICKER;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_COLUMN;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_LABEL;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_LABEL_CG;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_SLIDER;
import static gologolo.workspace.style.GLGLStyle.CLASS_GLGL_TABLE;
import java.time.LocalDate;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;

/**
 *
 * @author liuxiao
 */
public class LoGoWorkspace extends AppWorkspaceComponent{
    ItemsController itemsController;
    
    public ItemsController getItemsController() {
        return itemsController;
    }
    
    public LoGoWorkspace(GoLoGoLoApp app) {
        super(app);
        initLayout();
        initFoolproofDesign();
        
        
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void initLayout() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        AppNodesBuilder tdlBuilder = app.getGUIModule().getNodesBuilder();
        
        
        //MAIN SCENE
        //HBox toDoListPane = tdlBuilder.buildHBox(GLGL_PANE,  null, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE, ENABLED);
        
        //BorderPane toDoListPane = new BorderPane();
        VBox itemsBox = tdlBuilder.buildVBox(GLGL_ITEM_PANE,workspace, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        //for table
        TableView<LoGoItemPrototype> itemsTable  = tdlBuilder.buildTableView(GLGL_ITEMS_TABLE_VIEW,itemsBox,null,   CLASS_GLGL_TABLE, HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  true);
        TableColumn orderColumn   = tdlBuilder.buildTableColumn(  GLGL_ORDER_COLUMN, itemsTable,         CLASS_GLGL_COLUMN);
        TableColumn nameColumn      = tdlBuilder.buildTableColumn(  GLGL_NAME_COLUMN,    itemsTable,         CLASS_GLGL_COLUMN);
        TableColumn typeColumn   = tdlBuilder.buildTableColumn(  GLGL_TYPE_COLUMN, itemsTable,         CLASS_GLGL_COLUMN);
        
        
        // AND NOW THE TABLE
        
        // SPECIFY THE TYPES FOR THE COLUMNS
        orderColumn.setCellValueFactory(     new PropertyValueFactory<String,    String>("order"));
        nameColumn.setCellValueFactory(  new PropertyValueFactory<String,    String>("name"));
        typeColumn.setCellValueFactory(       new PropertyValueFactory<String,    String>("type"));//
        
        
        //小toolbar
        HBox toolButtonsPane        = tdlBuilder.buildHBox(GLGL_ITEM_BUTTONS_PANE,          itemsBox,          null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Button moveUpItemButton       = tdlBuilder.buildIconButton(GLGL_MOVEUP_ITEM_BUTTON,   toolButtonsPane,    null,   CLASS_GLGL_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button moveDownItemButton       = tdlBuilder.buildIconButton(GLGL_MOVEDOWN_ITEM_BUTTON,   toolButtonsPane,    null,   CLASS_GLGL_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button editItemButton       = tdlBuilder.buildIconButton(GLGL_EDIT_ITEM_BUTTON,   toolButtonsPane,    null, CLASS_GLGL_BUTTON , HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        
        
        //VBox center = tdlBuilder.buildVBox(GLGL_CENTER_PANE,workspace, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        
        StackPane center = new StackPane();
        center.getStyleClass().add(CLASS_GLGL_CENTER_PANE);
       

        //在right里面
        //右边上面管文字
        //VBox rightFText = tdlBuilder.buildVBox(GLGL_RIGHT_TEXT_PANE,right, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        VBox right = tdlBuilder.buildVBox(GLGL_RIGHT_PANE,workspace, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        HBox rightComponent = tdlBuilder.buildHBox(GLGL_RIGHT_COMPONENT_PANE,right,null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Button addTextButton       = tdlBuilder.buildIconButton(GLGL_ADD_ITEM_BUTTON,   rightComponent,    null,   CLASS_GLGL_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button addImageButton       = tdlBuilder.buildIconButton(GLGL_IMAGE_BUTTON,   rightComponent,    null,   CLASS_GLGL_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button recButton       = tdlBuilder.buildIconButton(GLGL_REC_BUTTON,   rightComponent,    null, CLASS_GLGL_BUTTON , HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button addcircleButton       = tdlBuilder.buildIconButton(GLGL_CIRCLE_BUTTON,   rightComponent,    null,   CLASS_GLGL_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button triangleButton       = tdlBuilder.buildIconButton(GLGL_TRIANGLE_BUTTON,   rightComponent,    null, CLASS_GLGL_BUTTON , HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button deleteButton       = tdlBuilder.buildIconButton(GLGL_DELETE_BUTTON,   rightComponent,    null, CLASS_GLGL_BUTTON , HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        
        //右边中间管图像部分
        VBox rightFImageM = tdlBuilder.buildVBox(GLGL_RIGHT_F_IMAGE_MAIN_PANE,right, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        HBox rightFImage = tdlBuilder.buildHBox(GLGL_RIGHT_IMAGE_PANE,rightFImageM,null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Button BoldButton       = tdlBuilder.buildIconButton(GLGL_B_BUTTON,   rightFImage,    null,   CLASS_GLGL_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button italicButton       = tdlBuilder.buildIconButton(GLGL_SLOPE_BUTTON,   rightFImage,    null, CLASS_GLGL_BUTTON , HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button BTLButton       = tdlBuilder.buildIconButton(GLGL_BTL_BUTTON,   rightFImage,    null,   CLASS_GLGL_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        Button LTBButton       = tdlBuilder.buildIconButton(GLGL_LTB_BUTTON,   rightFImage,    null, CLASS_GLGL_BUTTON , HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);//
        
        //右边中间管图像choicebox部分
        HBox textTypeMain = tdlBuilder.buildHBox(GLGL_RIGHT_IMAGE_PANE,rightFImageM,null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        ComboBox textType = tdlBuilder.buildComboBox(GLGL_TEXTTYPE_COMBOBOX, GLGL_OPTIONLISTS_COMBOBOX, GLGL_DEFAULT_TYPE, textTypeMain, null, CLASS_GLGL_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ComboBox textSize = tdlBuilder.buildComboBox(GLGL_TEXTSIZE_COMBOBOX,GLGL_OPTIONLISTSSIZE_COMBOBOX, GLGL_DEFAULT_SIZE, textTypeMain, null, CLASS_GLGL_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker textColor = tdlBuilder.buildColorPicker(GLGL_FOR_TEXT_COLOR_PICKER, textTypeMain,null, CLASS_GLGL_COLOR_PICKER,  HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        
        //右边FOR BORDER
        VBox rightFBorders = tdlBuilder.buildVBox(GLGL_RIGHT_BORDER_PANE,right, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Label border_thickness = tdlBuilder.buildLabel(GLGL_BORDER_THICKNESS_LABEL, rightFBorders, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider ForBorder_thickness = tdlBuilder.buildSlider(GLGL_FOR_BORDER_THICKNESS_SILDER, rightFBorders, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label border_color = tdlBuilder.buildLabel(GLGL_BORDER_COLOR_LABEL, rightFBorders, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker for_border_color = tdlBuilder.buildColorPicker(GLGL_FOR_BORDER_COLOR_PICKER, rightFBorders,null, CLASS_GLGL_COLOR_PICKER,  HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label border_radius = tdlBuilder.buildLabel(GLGL_BORDER_RADIUS_LABEL, rightFBorders, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider ForBorder_radius = tdlBuilder.buildSlider(GLGL_FOR_BORDER_RADIUS_SILDER, rightFBorders, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        //下面的color gradient
        VBox rightFColor = tdlBuilder.buildVBox(GLGL_RIGHT_COLOR_PANE,right, null,   CLASS_GLGL_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Label color_gradient = tdlBuilder.buildLabel(GLGL_COLOR_GRADIENT_LABEL, rightFColor, null, CLASS_GLGL_LABEL_CG, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Label focus_angle = tdlBuilder.buildLabel(GLGL_FOCUS_ANGLE_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider Forfocus_angle = tdlBuilder.buildSlider(GLGL_FOR_FOCUS_ANGLE_SILDER, rightFColor, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Label focus_distance = tdlBuilder.buildLabel(GLGL_FOCUS_DISTANCE_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider Forfocus_distance = tdlBuilder.buildSlider(GLGL_FOR_DISTANCE_SILDER, rightFColor, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        
        Label center_x = tdlBuilder.buildLabel(GLGL_CENTERX_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider Forcenter_x = tdlBuilder.buildSlider(GLGL_FOR_CENTERX_SILDER, rightFColor, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label center_y = tdlBuilder.buildLabel(GLGL_CENTERY_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider Forcenter_y = tdlBuilder.buildSlider(GLGL_FOR_CENTERY_SILDER, rightFColor, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label radius = tdlBuilder.buildLabel(GLGL_RADIUS_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider Forradius = tdlBuilder.buildSlider(GLGL_FOR_RADIUSE_SILDER, rightFColor, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Label Cycle_Method = tdlBuilder.buildLabel(GLGL_CYCLE_METHOD_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ComboBox FCycle_Method = tdlBuilder.buildComboBox(GLGL_FCYCLE_METHOD_COMBOBOX, GLGL_OPTIONLISTS_FCYCLE_METHOD_COMBOBOX, GLGL_DEFAULT_METHOD, rightFColor, null, CLASS_GLGL_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Label step0_Color = tdlBuilder.buildLabel(GLGL_STEP0_COLOR_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker for_step0_Color = tdlBuilder.buildColorPicker(GLGL_FOR_STEP0_COLOR_PICKER, rightFColor,null, CLASS_GLGL_COLOR_PICKER,  HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label step1_Color = tdlBuilder.buildLabel(GLGL_STEP1_COLOR_LABEL, rightFColor, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker for_step1_Color = tdlBuilder.buildColorPicker(GLGL_FOR_STEP1_COLOR_PICKER, rightFColor,null, CLASS_GLGL_COLOR_PICKER,  HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        //for navigation toolbar
        ToolBar navigationToolBar = new ToolBar();
        app.getGUIModule().getTopToolbarPane().getChildren().add(navigationToolBar);
        CheckBox snap = tdlBuilder.buildCheckBox(SNAP_CHECK_BOX, null, navigationToolBar, CLASS_DJF_CHECK_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label forSnap = tdlBuilder.buildLabel(SNAP_LABEL, null, navigationToolBar, CLASS_SNAP_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button resizeLog  = tdlBuilder.buildIconButton(RESIZE_LOGO_BUTTON, null, navigationToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button zoomInButton  = tdlBuilder.buildIconButton(ZOOM_IN_BUTTON, null, navigationToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button zoomOutButton  = tdlBuilder.buildIconButton(ZOOM_OUT_BUTTON, null, navigationToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button resetViewport = tdlBuilder.buildIconButton(RESET_VIEW_PORT_BUTTON, null, navigationToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        workspace = new BorderPane();
        ((BorderPane)workspace).setCenter(center);
        ((BorderPane)workspace).setLeft(itemsBox);
        ((BorderPane)workspace).setRight(right);
        
        itemsController = new ItemsController((GoLoGoLoApp)app);
        
        resizeLog.setOnAction(e->{
            itemsController.processChangeDimension();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        zoomInButton.setOnAction(e->{
            itemsController.processAllcomponentDouble();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        zoomOutButton.setOnAction(eh->{
            itemsController.processAllcomponentHalf();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            });
        
        resetViewport.setOnAction(eh->{
            itemsController.processAllNormalSize();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            });
        
        addTextButton.setOnAction(e->{
            itemsController.processAddText();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            
        });
        
        addImageButton.setOnAction(e->{
            itemsController.processAddImage();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            
        });
        
        recButton.setOnAction(e->{
            itemsController.processAddRec();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            
        });
        deleteButton.setOnAction(e->{
            itemsController.processDelete();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            
        });
        itemsTable.setOnMouseClicked(e->{
            
            itemsController.setNodeHighlight();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        moveUpItemButton.setOnMouseClicked(e->{
            itemsController.processMoveUp();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        moveDownItemButton.setOnMouseClicked(e->{
            itemsController.processMoveDown();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        editItemButton.setOnMouseClicked(e->{
            itemsController.processEditName();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        addcircleButton.setOnAction(e->{
            itemsController.processAddCircle();
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        BoldButton.setOnAction(e->{
            
            itemsController.processTextFont((String)textType.getSelectionModel().getSelectedItem(), Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()),true,false);
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            
        });
        
        italicButton.setOnAction(e->{
            
            itemsController.processTextFont((String)textType.getSelectionModel().getSelectedItem(), Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()),false,true);
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        textType.setOnAction(e->{
            
            itemsController.processTextFont((String)textType.getSelectionModel().getSelectedItem(), Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()),false,false);
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
//        textSize.setOnAction(e->{
//            
//            itemsController.processTextFont((String)textType.getSelectionModel().getSelectedItem(), Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()),false,false);
//        
//        });
        textSize.getSelectionModel().selectedItemProperty().addListener(e->{
            itemsController.processTextFont((String)textType.getSelectionModel().getSelectedItem(), Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()),false,false);
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
//        BTLButton.setOnAction(e->{
//            
//            itemsController.processTextFont((String)textType.getSelectionModel().getSelectedItem(), Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()) ,false,false);
//        
//        });
//        
//        LTBButton.setOnAction(e->{
//            
//            itemsController.processTextFont((String)textType.getSelectionModel().getSelectedItem(), Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()) ,false,false);
//        
//        });

        LTBButton.setOnAction(e->{
            textSize.getSelectionModel().select("" + (Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()) + 1));
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        BTLButton.setOnAction(e->{
            textSize.getSelectionModel().select("" + (Integer.parseInt((String)textSize.getSelectionModel().getSelectedItem()) + 1));
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        textColor.setOnAction(e->{
            itemsController.changeTextColor(textColor.getValue());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        ForBorder_thickness.setOnMouseReleased(e->{
            
            itemsController.changeBorderThickness(ForBorder_thickness.getValue());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        ForBorder_thickness.setOnMouseDragged(e->{
            
            itemsController.changeBorderThickness2(ForBorder_thickness.getValue());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        for_border_color.setOnAction(e->{
            itemsController.changeBorderColor(for_border_color.getValue());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        ForBorder_radius.setOnMouseReleased(e->{
            
            itemsController.changeRadiusThickness(ForBorder_radius.getValue());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        ForBorder_radius.setOnMouseDragged(e->{
            itemsController.changeRadiusThickness1(ForBorder_radius.getValue());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        
        
        Forfocus_angle.setOnMouseReleased(e->{
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forfocus_distance.setOnMouseReleased(e->{
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forcenter_x.setOnMouseReleased(e->{
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forcenter_y.setOnMouseReleased(e->{
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forradius.setOnMouseReleased(e->{
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        FCycle_Method.setOnAction(e->{
            
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            
        });
        
        for_step0_Color.setOnAction(e->{
            
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
            
        });
        
        for_step1_Color.setOnAction(e->{
            
            itemsController.changeColorRadius(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        //只是为了展示用
        Forfocus_angle.setOnMouseDragged(e->{
            itemsController.changeColorRadius1(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forfocus_distance.setOnMouseDragged(e->{
            itemsController.changeColorRadius1(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forcenter_x.setOnMouseDragged(e->{
            itemsController.changeColorRadius1(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forcenter_y.setOnMouseDragged(e->{
            itemsController.changeColorRadius1(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
        Forradius.setOnMouseDragged(e->{
            itemsController.changeColorRadius1(getComponentFRG());
            app.getFileModule().markAsEdited(true);
            app.getFoolproofModule().updateAll();
        });
        
    }
        
        public RadialGradient getComponentFRG(){
            AppGUIModule gui = app.getGUIModule();
            double fFocus = ((Slider)gui.getGUINode(GLGL_FOR_FOCUS_ANGLE_SILDER)).getValue()*3.6;
            double fDistance = ((Slider)gui.getGUINode(GLGL_FOR_DISTANCE_SILDER)).getValue()/100;
            double centerX = ((Slider)gui.getGUINode(GLGL_FOR_CENTERX_SILDER)).getValue()/100;
            double centerY = ((Slider)gui.getGUINode(GLGL_FOR_CENTERY_SILDER)).getValue()/100;
            double radius = ((Slider)gui.getGUINode(GLGL_FOR_RADIUSE_SILDER)).getValue()/100;
            boolean proportional = true;
            CycleMethod cycleMethod = CycleMethod.valueOf((String)((ComboBox)gui.getGUINode(GLGL_FCYCLE_METHOD_COMBOBOX)).getValue());
            Stop stop0 = new Stop(0, ((ColorPicker)gui.getGUINode(GLGL_FOR_STEP0_COLOR_PICKER)).getValue());
            Stop stop1 = new Stop(1, ((ColorPicker)gui.getGUINode(GLGL_FOR_STEP1_COLOR_PICKER)).getValue());
            Stop[] stops = {stop0 , stop1};
            
            RadialGradient newOne = new RadialGradient(fFocus,fDistance,centerX,centerY,radius,proportional,cycleMethod,stops);
            return newOne;
   }
                    
    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(GLGL_FOOLPROOF_SETTINGS, 
                new LoGoSelectionFoolproofDesign((GoLoGoLoApp)app));

    }
}
