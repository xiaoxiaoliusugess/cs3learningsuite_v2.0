/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.clipboard;

import djf.components.AppClipboardComponent;
import gologolo.GoLoGoLoApp;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import gologolo.transactions.CutItems_Transaction;
import gologolo.transactions.PasteItems_Transaction;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 *
 * @author liuxiao
 */
public class LoGoClipboard implements AppClipboardComponent{
    GoLoGoLoApp app;
    LoGoItemPrototype clipboardCutItems;
    LoGoItemPrototype clipboardCopiedItems;
    static LoGoItemPrototype temp; 
    static Text text;
    static Rectangle rec2;

    public LoGoClipboard(GoLoGoLoApp initApp) {
        app = initApp;
        clipboardCutItems = null;
        clipboardCopiedItems = null;
    }

    @Override
    public void cut() {
        LoGoData data = (LoGoData)app.getDataComponent();
        if (data.isItemSelected()) {
            clipboardCutItems = data.getSelectedItem();
            clipboardCopiedItems = null;
            CutItems_Transaction transaction = new CutItems_Transaction((GoLoGoLoApp)app, clipboardCutItems);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
        }
    }

    @Override
    public void copy() {
        LoGoData data = (LoGoData)app.getDataComponent();
        if (data.isItemSelected()) {
            LoGoItemPrototype tempItems = data.getSelectedItem();
            System.out.println(tempItems.getType() + "copy有没有");
            copyToCopiedClipboard(tempItems);
        }
    }
    

    @Override
    public void paste() {
        LoGoData data = (LoGoData)app.getDataComponent();
        LoGoItemPrototype pasteItems = clipboardCutItems;
        if (data.isItemSelected()) {
            int selectedIndex = data.getItemIndex(data.getSelectedItem());  
             if (clipboardCutItems != null) {
                
                //System.out.println("1234567890");
                PasteItems_Transaction transaction = new PasteItems_Transaction
                ((GoLoGoLoApp)app, pasteItems, selectedIndex);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
                
                // NOW WE HAVE TO RE-COPY THE CUT ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                copyToCutClipboard(clipboardCopiedItems);
            }
            else if (clipboardCopiedItems != null) {
                //LoGoItemPrototype pasteItems2 = getNodeInclipboardCopiedItems(data);
                PasteItems_Transaction transaction = new PasteItems_Transaction
                ((GoLoGoLoApp)app, clipboardCopiedItems, selectedIndex);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
            
                // NOW WE HAVE TO RE-COPY THE COPIED ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                copyToCopiedClipboard(clipboardCopiedItems);
            }
        }
        //table里面没东西了
        else{
            if (clipboardCutItems != null) {
                //System.out.println("1234567890");
                PasteItems_Transaction transaction = new PasteItems_Transaction
                ((GoLoGoLoApp)app, pasteItems, -1);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
            }
        }
        
    }    

    @Override
    public boolean hasSomethingToCut() {
        return ((LoGoData)app.getDataComponent()).isItemSelected()
                || ((LoGoData)app.getDataComponent()).areItemsSelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((LoGoData)app.getDataComponent()).isItemSelected()
                || ((LoGoData)app.getDataComponent()).areItemsSelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        if ((clipboardCutItems != null) && (clipboardCutItems!= null))
            return true;
        else if ((clipboardCopiedItems != null) && (clipboardCopiedItems!= null))
            return true;
        else
            return false;
    }

    private void copyToCopiedClipboard(LoGoItemPrototype itemsToCopy) {
        clipboardCutItems = null;
        clipboardCopiedItems = (LoGoItemPrototype)itemsToCopy.clone();   /////     
        app.getFoolproofModule().updateAll();     
        //System.out.println(clipboardCopiedItems.getType() + "copy板有没有");
    }

     private void copyToCutClipboard(LoGoItemPrototype itemsToCopy) {
        clipboardCutItems = (LoGoItemPrototype)itemsToCopy.clone();
        clipboardCopiedItems = null;        
        app.getFoolproofModule().updateAll();        
    }

}

            
        
                
      

    

