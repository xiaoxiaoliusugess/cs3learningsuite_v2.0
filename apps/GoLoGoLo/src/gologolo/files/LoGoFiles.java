/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.files;

import static djf.AppTemplate.PATH_WORK;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import gologolo.data.LoGoData;
import gologolo.data.LoGoItemPrototype;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author liuxiao
 */
public class LoGoFiles implements AppFileComponent{
    static final String JSON_ORDER = "order";
    static final String JSON_NAME = "name";
    static final String JSON_TYPE = "type";
    static final String JSON_WIDTH = "width";//
    static final String JSON_HEIGHT = "height";
    static final String JSON_NODES = "node";//不确定
    static final String JSON_ITEMS = "items";
    
    //for text
    static final String JSON_TEXT = "text";
    static final String JSON_TEXT_X = "x";
    static final String JSON_TEXT_Y = "y";
    static final String JSON_ALIGMENT = "aligment";
    static final String JSON_ORIGIN = "origin";
    static final String JSON_BOUNDSTYPE = "boundsType";
    static final String JSON_ISBOLD = "isBold";
    static final String JSON_ISITALIC = "isIsitalic";
    static final String JSON_FILL = "fill";
    static final String JSON_FAMILYNAME = "familyname";
    static final String JSON_SIZE = "size";
    
    //for shapes
    //for rectangle 
    static final String JSON_FILL_FOR_REC = "fill";
    static final String JSON_REC_X = "x";
    static final String JSON_REC_Y = "y";
    static final String JSON_REC_TX = "Tx";
    static final String JSON_REC_TY = "Ty";
    static final String JSON_WIDTH_REC = "width";
    static final String JSON_HEIGHT_REC = "height";
    static final String JSON_STROKE_REC = "stroke";
    static final String JSON_STROKE_WIDTH_REC = "strokeWidth";
    static final String JSON_REC_ARC_HEIGHT = "arcHeight";
    static final String JSON_REC_ARC_WIDTH = "arcWidth";
    
    
    //for circle
    static final String JSON_CENTER_X = "centerX";
    static final String JSON_CENTER_Y = "centerY";
    static final String JSON_RADIUS = "radius";
    static final String JSON_FILL_FOR_CIRCLE = "fill";
    static final String JSON_STROKE_CIRCLE = "stroke";
    static final String JSON_STROKE_WIDTH_CIRCLE = "strokeWidth";
    
    //for image
    static final String JSON_URL = "URL";
    static final String JSON_IMAGEX = "x";
    static final String JSON_IMAGEY = "y";
    
    //for huanbu
    static final String JSON_COLORR = "Color gradient";
    
   public JsonObject saveinnerNode(Node node, LoGoData data) {
        LoGoItemPrototype temp = (LoGoItemPrototype)node.getUserData();
        JsonObject itemJson = null;
         
        if(temp.getType().equals("Rectangle")){
            itemJson = Json.createObjectBuilder()
                    .add(JSON_REC_X,((Rectangle)node).getX()+ "")
                    .add(JSON_REC_Y,((Rectangle) node).getY()+ "")
                    .add(JSON_REC_TX,((Rectangle)node).getTranslateX()+ "")
                    .add(JSON_REC_TY,((Rectangle) node).getTranslateY()+ "")
                    .add(JSON_WIDTH_REC, ((Rectangle) node).getWidth() + "")
                    .add(JSON_HEIGHT_REC,((Rectangle) node).getHeight() + "")
                    .add(JSON_REC_ARC_HEIGHT,((Rectangle) node).getArcHeight() + "")
                    .add(JSON_REC_ARC_WIDTH,((Rectangle) node).getArcWidth() + "")
                    .add(JSON_STROKE_REC,((Rectangle) node).getStroke().toString())
                    .add(JSON_STROKE_WIDTH_REC,Double.toString( ((Rectangle) node).getStrokeWidth()))
                    .add(JSON_FILL_FOR_REC,((RadialGradient)((Rectangle) node).getFill()).toString())
                    .build();
	    
            return itemJson; 
        }
        else if(temp.getType().equals("Text")){
            itemJson = Json.createObjectBuilder()
                    .add(JSON_TEXT,((Text) node).getText() )
                    .add(JSON_TEXT_X, ((Text) node).getX()+"")
                    .add(JSON_TEXT_Y, ((Text) node).getY()+"")
                    .add(JSON_REC_TX,((Text)node).getTranslateX()+ "")
                    .add(JSON_REC_TY,((Text) node).getTranslateY()+ "")
                    .add(JSON_ALIGMENT, "LEFT")
                    .add(JSON_ORIGIN, "BASELINE")
                    .add(JSON_BOUNDSTYPE, "LOGICAL")
                    .add(JSON_ISBOLD, temp.isIsBold())
                    .add(JSON_ISITALIC, temp.isIsItalic())
                    .add(JSON_FILL, ((Text) node).getFill().toString())
                    .add(JSON_FAMILYNAME, ((Text) node).getFont().getFamily().toString())
                    .add(JSON_SIZE,Double.toString(((Text) node).getFont().getSize()))
                    .build();
            return itemJson; 
        
        }
        else if(temp.getType().equals("Circle")){
           itemJson = Json.createObjectBuilder()
                    .add(JSON_CENTER_X,((Circle)node).getCenterX()+"")
                    .add(JSON_CENTER_Y,((Circle)node).getCenterY()+"")
                    .add(JSON_REC_TX,((Circle)node).getTranslateX()+ "")
                    .add(JSON_REC_TY,((Circle) node).getTranslateY()+ "")
                    .add(JSON_RADIUS,((Circle)node).getRadius()+"")
		    .add(JSON_FILL_FOR_CIRCLE,((RadialGradient)((Circle) node).getFill()).toString())
                    .add(JSON_STROKE_CIRCLE,((Circle) node).getStroke().toString())
                    .add(JSON_STROKE_WIDTH_CIRCLE,Double.toString( ((Circle) node).getStrokeWidth()))
                    .build();
	    
            return itemJson; 
        }
        else if(temp.getType().equals("Image")){
           itemJson = Json.createObjectBuilder()
                    .add(JSON_IMAGEX,node.getTranslateX()+"")
                    .add(JSON_IMAGEY,node.getTranslateY()+"")
                    .build();
            return itemJson; 
                   }
        return itemJson; 
        
        
    }


    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        LoGoData toDoData = (LoGoData)data;
        
        String width = Double.toString(toDoData.getCanvas().getWidth());
        String height = Double.toString(toDoData.getCanvas().getHeight());
        String colorR = toDoData.getCanvas().getBackground().getFills().get(0).getFill().toString();
        
        
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        Iterator<LoGoItemPrototype> itemsIt = toDoData.itemsIterator();
	while (itemsIt.hasNext()) {	
            LoGoItemPrototype item = itemsIt.next();
            
            if(item.getUrl()==null){
                JsonObject itemJson = Json.createObjectBuilder()
                        .add(JSON_ORDER, item.getOrder()+ "")
                        .add(JSON_NAME, item.getName())
                        .add(JSON_TYPE, item.getType())
                        .add(JSON_NODES, saveinnerNode(item.getNode(),toDoData))
                        .build();
                arrayBuilder.add(itemJson);
            }
            else{
                JsonObject itemJson = Json.createObjectBuilder()
                        .add(JSON_ORDER, item.getOrder()+ "")
                        .add(JSON_NAME, item.getName())
                        .add(JSON_TYPE, item.getType())
                        .add(JSON_URL, item.getUrl())
                        .add(JSON_NODES, saveinnerNode(item.getNode(),toDoData))
                        .build();
                arrayBuilder.add(itemJson);
            }
        }
	JsonArray itemsArray = arrayBuilder.build();
        JsonObject toDoDataJSO = Json.createObjectBuilder()
                .add(JSON_WIDTH, width)
                .add(JSON_HEIGHT, height)
                .add(JSON_COLORR,colorR)
		.add(JSON_ITEMS, itemsArray)//不懂什么意思
		.build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(toDoDataJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(toDoDataJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
        
    
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        LoGoData toDoData = (LoGoData)data;
	toDoData.reset();

	JsonObject json = loadJSONFile(filePath);
        double width = Double.parseDouble(json.getString(JSON_WIDTH));//
	double height = Double.parseDouble(json.getString(JSON_HEIGHT));
        toDoData.reSize(width, height);
        
        Paint paint = Paint.valueOf(json.getString(JSON_COLORR));
        toDoData.getBackground().setBackground(new Background(new BackgroundFill(paint,null,null)));
        
        JsonArray jsonItemArray = json.getJsonArray(JSON_ITEMS);
	for (int i = 0; i < jsonItemArray.size(); i++) {
	    JsonObject jsonItem = jsonItemArray.getJsonObject(i);
	    LoGoItemPrototype item = loadItem(jsonItem);
	   // toDoData.addItem(item);
              toDoData.addItemWithoutTrancation(item);
	}
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
        WritableImage image = ((LoGoData)data).getBackground().snapshot(new SnapshotParameters(), null);
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        // TODO: probably use a file chooser here
        File file = fc.showSaveDialog(null);

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            // TODO: handle exception here
        }
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

     private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
     public LoGoItemPrototype loadItem(JsonObject jsonItem) {
	// GET THE DATA
        int order = Integer.parseInt(jsonItem.getString(JSON_ORDER));
        String name = jsonItem.getString(JSON_NAME);
        String type = jsonItem.getString(JSON_TYPE);// 
        JsonObject node = jsonItem.getJsonObject(JSON_NODES); ///
	LoGoItemPrototype item  = new LoGoItemPrototype();
        
        
        if(!jsonItem.containsKey(JSON_URL)){
            
            if(type.equals("Text")){
                Text text = new Text();
                text.setText(node.getString(JSON_TEXT));
                text.setX(Double.parseDouble(node.getString(JSON_TEXT_X)));
                text.setY(Double.parseDouble(node.getString(JSON_TEXT_Y)));
                text.setTranslateX(Double.parseDouble(node.getString(JSON_REC_TX)));
                text.setTranslateY(Double.parseDouble(node.getString(JSON_REC_TY)));
                text.setTextAlignment(TextAlignment.valueOf(node.getString(JSON_ALIGMENT)));
                text.setTextOrigin(VPos.valueOf(node.getString(JSON_ORIGIN)));
                text.setBoundsType(TextBoundsType.valueOf(node.getString(JSON_BOUNDSTYPE)));
                text.setFill(Paint.valueOf(node.getString(JSON_FILL)));
                String familyname = node.getString(JSON_FAMILYNAME);
                double size = Double.parseDouble(node.getString(JSON_SIZE));
                Font f; 
                FontWeight weight; 
                FontPosture pos; 
                if(node.getBoolean(JSON_ISBOLD)){
                    weight = FontWeight.BOLD;
                }
                else{
                    weight = FontWeight.NORMAL;
                }
                if(node.getBoolean(JSON_ISITALIC)){
                    pos = FontPosture.ITALIC;
                }
                else{
                    pos = FontPosture.REGULAR;
                }

                f = Font.font(familyname, weight, pos, size);
                text.setFont(f);
                item = new LoGoItemPrototype(order, name, type, (Node)text);
            }

            else if (type.equals("Rectangle")){
                RadialGradient r;
                Rectangle rec = new Rectangle();
                rec.setX(Double.parseDouble(node.getString(JSON_REC_X)));
                rec.setY(Double.parseDouble(node.getString(JSON_REC_Y)));
                rec.setTranslateX(Double.parseDouble(node.getString(JSON_REC_TX)));
                rec.setTranslateY(Double.parseDouble(node.getString(JSON_REC_TY)));
                rec.setWidth(Double.parseDouble(node.getString(JSON_WIDTH_REC)));
                rec.setHeight(Double.parseDouble(node.getString(JSON_HEIGHT_REC)));
                rec.setArcHeight(Double.parseDouble(node.getString(JSON_REC_ARC_HEIGHT)));
                rec.setArcWidth(Double.parseDouble(node.getString(JSON_REC_ARC_WIDTH)));
                rec.setStrokeWidth(Double.parseDouble(node.getString(JSON_STROKE_WIDTH_REC)));
                rec.setStroke(Paint.valueOf(node.getString(JSON_STROKE_REC)));

                r = RadialGradient.valueOf(node.getString(JSON_FILL_FOR_CIRCLE));

                rec.setFill(r);

              item = new LoGoItemPrototype(order, name, type, (Node) rec);//

            }
        
        
            else if (type.equals("Circle")){
                RadialGradient r;
                Circle rec = new Circle();
                rec.setCenterX(Double.parseDouble(node.getString(JSON_CENTER_X)));
                rec.setCenterY(Double.parseDouble(node.getString(JSON_CENTER_Y)));
                rec.setTranslateX(Double.parseDouble(node.getString(JSON_REC_TX)));
                rec.setTranslateY(Double.parseDouble(node.getString(JSON_REC_TY)));
                rec.setRadius(Double.parseDouble(node.getString(JSON_RADIUS)));
                rec.setStrokeWidth(Double.parseDouble(node.getString(JSON_STROKE_WIDTH_CIRCLE)));
                rec.setStroke(Paint.valueOf(node.getString(JSON_STROKE_CIRCLE)));

                r = RadialGradient.valueOf(node.getString(JSON_FILL_FOR_CIRCLE));

                rec.setFill(r);

              item = new LoGoItemPrototype(order, name, type, (Node) rec);//

            } 
            return item;
        }
        else{
            String url = jsonItem.getString(JSON_URL);
            ImageView iv = new ImageView(new Image(url));
            iv.setTranslateX(Double.parseDouble(node.getString(JSON_IMAGEX)));
            iv.setTranslateY(Double.parseDouble(node.getString(JSON_IMAGEY)));
            item = new LoGoItemPrototype(order, name, type, (Node)iv, url);
            return item;
        }
     }    
    
}
