///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package djf.ui.controllers;
//
//import djf.AppTemplate;
//import djf.ui.dialogs.AppChangeDimensionDialog;
//import djf.ui.dialogs.AppDialogsFacade;
//import javafx.scene.layout.BorderPane;
//import javafx.scene.layout.StackPane;
//import javafx.scene.shape.Rectangle;
//
///**
// *
// * @author liuxiao
// */
//public class AppNavigationController {
//    AppTemplate app;
//    AppChangeDimensionDialog dialog;
//    
//    public AppNavigationController(AppTemplate initApp) {
//        app = initApp;
//        dialog = new AppChangeDimensionDialog(app);
//    }
//    
//    public void processChangeDimension(){
//        
//        dialog.showChangeDialog();
//        double height = dialog.getHeight();
//        double width = dialog.getWidth();
//        reSize(width,height);
//        
//    }
//    
//    public void reSize(double width, double height){
//        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).setMaxSize(width, height);
//        
//    }
//
//    public void processAllcomponentDouble() {
//        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).setScaleX(2);
//        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).setScaleY(2);
//        int num = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().size();
//        for(int i = 0;i < num; i++){
//            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().get(i).setScaleX(2);
//            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().get(i).setScaleY(2);
//        }
//    }
//
//    public void processAllcomponentHalf() {
//        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).setScaleX(0.5);
//        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).setScaleY(0.5);
//        int num = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().size();
//        for(int i = 0;i < num; i++){
//            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().get(i).setScaleX(0.5);
//            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().get(i).setScaleY(0.5);
//        }
//    }
//
//    public void processAllNormalSize() {
//    ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).setScaleX(1);
//        ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).setScaleY(1);
//        int num = ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().size();
//        for(int i = 0;i < num; i++){
//            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().get(i).setScaleX(1);
//            ((StackPane)((StackPane)((BorderPane)app.getWorkspaceComponent().getWorkspace())
//                .getCenter()).getChildren().get(0)).getChildren().get(i).setScaleY(1);
//        }
//    }
//}
